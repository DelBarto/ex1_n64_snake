#!/bin/bash

dirs_with_no_vhdl_files="
.
vhdl
"

ipcores="
audio_cntrl
math
n64_controller
prng
ram
serial_port
snake
snake_game
ssd_cntrl
synchronizer
textmode_controller
top
uart_n64_bridge
"

modules_with_makefiles="
top
ssd_cntrl
textmode_controller
prng
n64_controller
"

if [ "$(echo $1)" == "ex2" ]; then
modules_with_makefiles="$modules_with_makefiles char_lcd_cntrl" 
ipcores="$ipcores char_lcd_cntrl"
fi;

error_counter=0
warning_counter=0


if [ ! -d vhdl ]; then 
	echo "Error: vhdl directory missing"; 
	let "error_counter++"
fi;
	
for core in $ipcores; do 
	if [ ! -d vhdl/$core ]; then 
		echo "Error: module [$core] missing from your source tree"; 
		let "error_counter++"
	fi;
done;

for i in $dirs_with_no_vhdl_files; do 
	for f in $i/*.vhd; do 
		if [ -e "$f" ]; then
			echo "Error: [$f] -- The folder [$i] MUST NOT contain VHDL files. VHDL files may only be put in the src folder of each module"; 
			let "error_counter++"
		fi;
		break;
	done;
done;

for core in $ipcores; do 
	if [ -e "vhdl/$core" ] && [ $(ls vhdl/$core | grep -i "\.vhd" | wc -l) -ne 0 ]; then
		echo "Error: The folder [vhdl/$core] MUST NOT contain VHDL files. VHDL files may only be put in the src or tb folder of each module"; 
	fi;

	for d in vhdl/$core/*; do 
		if [ -d "${d}" ] && [ "$(basename $d)" != "src" ] && [ "$(basename $d)" != "tb" ] ; then
			if [ $(ls $d | grep -i ".vhd" | wc -l) -ne 0 ]; then
				echo "Error: The folder [$d] MUST NOT contain VHDL files. VHDL files may only be put in the src or tb folder of each module"; 
			fi;
		fi
	done;
done;


#check makefiles
for i in $modules_with_makefiles; do 
	if [ ! -e vhdl/$i/Makefile ]; then
		echo "Warning: The makefile for $i module is missing!";
		let "warning_counter++"
	else
		for target in compile sim clean; do
			if ! [[ $(cat vhdl/$i/Makefile | grep "^$target:") ]]; then
				echo "Warning: The makefile for $i module is missing the $target target!";
				let "warning_counter++"
			fi;
		done;
	fi;
done;

#check for quartus project
for f in vhdl/top/quartus/*.qpf; do 
	if [ ! -e "$f" ]; then
		echo "Error: Quartus project missing!";
		let "error_counter++"
	fi;
	break;
done;

#check for SDC file in quartus project directory
for f in vhdl/top/quartus/*.sdc; do 
	if [ ! -e "$f" ]; then
		echo "Warning: SDC file missing in top-level quartus project directory vhdl/top/quartus!";
		let "warning_counter++"
	fi;
	break;
done;

#check for PLL file in top source directory
if [ $(ls vhdl/top/src/ | grep -i PLL | wc -l) -eq 0 ]; then
	echo "Error: PLL file missing from /top/src directory!";
	let "error_counter++"
fi;

#check for the all keyword
for f in vhdl/ssd_cntrl/src/*.vhd; do 
	if [ -e "$f" ]; then
		if [ $(cat vhdl/ssd_cntrl/src/*.vhd | grep process | grep all | wc -l) -ne 0 ]; then 
			echo "Warning: Your ssd_cntrl seems to use the all keyword for sensitivity lists";
		fi;
	fi;
	break;
done;

exit $error_counter

