vlib work
vmap work work 


onerror {resume}
# quietly WaveActivateNextPane {} 0
#add wave -noupdate -divider Signals
add wave -noupdate /top_tb/nclk
add wave -noupdate /top_tb/vd
add wave -noupdate /top_tb/hd
add wave -noupdate /top_tb/den
add wave -noupdate /top_tb/r
add wave -noupdate /top_tb/g
add wave -noupdate /top_tb/b
add wave -noupdate /top_tb/hex6
add wave -noupdate /top_tb/hex7
