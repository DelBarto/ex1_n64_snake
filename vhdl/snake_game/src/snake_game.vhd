library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.snake_pkg.all;
use work.textmode_controller_pkg.all;
use work.display_controller_pkg.all;
use work.math_pkg.all;
use work.n64_controller_pkg.all;
use work.audio_cntrl_pkg.all;
use work.snake_pkg.all;
use work.snake_game_pkg.all;
use work.prng_pkg.all;

entity snake_game is
	port (
		clk : in std_logic;
		res_n : in std_logic;
		
		--connection to textmode controller
		instr_wr     : out std_logic;
		instr        : out std_logic_vector(3 downto 0);
		instr_data   : out std_logic_vector(15 downto 0);
		instr_result : in std_logic_vector(15 downto 0);
		busy         : in std_logic;
		vblank       : in std_logic;
		
		--connections to the N64 controllers
		cntrl1 : in n64_buttons_t;
		cntrl2 : in n64_buttons_t;
		
		--connection to audio controllers
		synth_cntrl : out synth_cntrl_vec_t(0 to 1);
		
		--connection to character LCD
		--TODO: Exercise II
		
		--misc 
		game_state : out snake_game_state_t
	); 
end entity;

architecture arch of snake_game is
	
	constant FRAMES_BETWEEN_MOVEMENT : integer := 6;
	
	type state_t is (
		RESET, INIT_SET_CFG, INIT_CLEAR_DISPLAY, INIT_WAIT_CLEAR,
		PLACE_SNAKE, PLACE_SNAKE_WAIT,
		MOVE_HEAD, WAIT_MOVE_HEAD, MOVE_TAIL, WAIT_MOVE_TAIL,
		WAIT_VBLANK, PROCESS_INPUTS
	);
	
	signal state : state_t;
	signal state_next : state_t;
	
	signal frame_counter : std_logic_vector(log2c(FRAMES_BETWEEN_MOVEMENT)-1 downto 0);
	signal frame_counter_next : std_logic_vector(frame_counter'range);
	
	signal prev_vblank : std_logic;
	
	
	--wires to snake initalizer
	signal snake_instr_wr : std_logic;
	signal snake_instr : std_logic_vector(3 downto 0);
	signal snake_instr_data : std_logic_vector(15 downto 0);
	signal snake_instr_result : std_logic_vector(15 downto 0);
	signal snake_done : std_logic;

	signal snake_init : std_logic;
	signal snake_move_head : std_logic;
	signal snake_move_tail : std_logic;
	
	signal movement_direction : direction_t;
	signal movement_direction_next : direction_t;
	
	signal rnd_vec : std_logic_vector(1 downto 0);
	signal skip_move : std_logic;
	signal skip_move_next : std_logic;
	
	signal load_seed : std_logic;
begin
	
	sync : process(clk, res_n)
	begin
		if (res_n = '0') then
			state <= RESET;
			frame_counter <= (others=>'0');
			prev_vblank <= '0';
			movement_direction <= UP;
			skip_move <= '0';
		elsif (rising_edge(clk)) then
			state <= state_next;
			frame_counter <= frame_counter_next;
			prev_vblank <= vblank;
			movement_direction <= movement_direction_next;
			skip_move <= skip_move_next;
		end if;
	end process;
	
	next_state_logic : process(all)
		variable tmp_dir : direction_t;
	begin
		tmp_dir := movement_direction;
		skip_move_next <= skip_move;
		load_seed <= '0';
		
		snake_move_head <= '0';
		snake_move_tail <= '0';
		snake_init <= '0';
		
		instr_wr   <= '0';
		instr      <= (others=>'0');
		instr_data <= (others=>'0');
	
		state_next <= state;
		frame_counter_next <= frame_counter;
	
		movement_direction_next <= movement_direction;
	
		case state is 
			when RESET =>
				load_seed <= '1';
				if(busy = '0') then
					state_next <= INIT_SET_CFG;
				end if;
				
			when INIT_SET_CFG =>
				instr_wr <= '1';
				instr <= INSTR_CFG;
				instr_data <= x"0" & COLOR_BLUE & x"0" & '1' & '0' & CURSOR_STATE_BLINK;
				state_next <= INIT_CLEAR_DISPLAY;
			
			when INIT_CLEAR_DISPLAY =>
				if (busy = '0') then
					instr_wr <= '1';
					instr <= INSTR_CLEAR_SCREEN;
					instr_data <= COLOR_BLACK & COLOR_BLACK & x"00";
					state_next <= INIT_WAIT_CLEAR;
				end if;
			
			when INIT_WAIT_CLEAR => 
				if (busy = '0') then
					state_next <= PLACE_SNAKE;
				end if;
			
			when PLACE_SNAKE =>
				snake_init <= '1';
				movement_direction_next <= LEFT;
				state_next <= PLACE_SNAKE_WAIT;
			
			when PLACE_SNAKE_WAIT =>
				instr_wr <= snake_instr_wr;
				instr <= snake_instr;
				instr_data <= snake_instr_data;
				if(snake_done = '1') then
					state_next <= WAIT_VBLANK;
				end if;
			
			when WAIT_VBLANK => 
				if (vblank = '1' and prev_vblank = '0') then
					if (unsigned(frame_counter) = FRAMES_BETWEEN_MOVEMENT-1) then
						frame_counter_next <= (others=>'0');
						state_next <= PROCESS_INPUTS;
					else
						frame_counter_next <= std_logic_vector(unsigned(frame_counter) + 1);
					end if;
				end if;
			
			when PROCESS_INPUTS =>
				movement_direction_next <= movement_direction;
				
				if (cntrl1.btn_z = '1') then -- controlled movement
					if (cntrl1.btn_up = '1' or cntrl2.btn_c_up = '1') then
						tmp_dir := UP;
					elsif (cntrl1.btn_down = '1' or cntrl2.btn_c_down = '1') then
						tmp_dir := DOWN;
					elsif (cntrl1.btn_left = '1' or cntrl2.btn_c_left = '1') then
						tmp_dir := LEFT;
					elsif (cntrl1.btn_right = '1' or cntrl2.btn_c_right = '1') then
						tmp_dir := RIGHT;
					end if;
					-- if we are going e.g. UP we cannot go DOWN for the next move
					if (tmp_dir /= not movement_direction) then
						movement_direction_next <= tmp_dir;
					end if;
					state_next <= MOVE_HEAD;
				elsif (cntrl1.btn_b = '1') then -- random movement
					case rnd_vec is
						when "00" => tmp_dir := UP;
						when "01" => tmp_dir := DOWN;
						when "10" => tmp_dir := LEFT;
						when others => tmp_dir := RIGHT;
					end case;
					--skip_move is used to prevent the snake from eating its own tail
					skip_move_next <= not skip_move;
					if (tmp_dir /= not movement_direction and skip_move = '0') then
						movement_direction_next <= tmp_dir;
					end if;
					state_next <= MOVE_HEAD;
				else
					state_next <= WAIT_VBLANK;
				end if;

			when MOVE_HEAD =>
				snake_move_head <= '1';
				state_next <= WAIT_MOVE_HEAD;
			
			when WAIT_MOVE_HEAD =>
				instr_wr <= snake_instr_wr;
				instr <= snake_instr;
				instr_data <= snake_instr_data;
				if(snake_done = '1') then
					state_next <= MOVE_TAIL;
				end if;
			
			when MOVE_TAIL => 
				snake_move_tail <= '1';
				state_next <= WAIT_MOVE_TAIL;
			
			when WAIT_MOVE_TAIL => 
				instr_wr <= snake_instr_wr;
				instr <= snake_instr;
				instr_data <= snake_instr_data;
				if(snake_done = '1') then
					state_next <= WAIT_VBLANK;
				end if;
			
			when others =>
				null;
		end case;
	end process;
	
	snake_inst : snake
	port map (
		clk             => clk,
		res_n           => res_n,
		tm_instr_wr     => snake_instr_wr,
		tm_instr        => snake_instr,
		tm_instr_data   => snake_instr_data,
		tm_busy         => busy,
		tm_instr_result => instr_result,
		color           => COLOR_BLUE & COLOR_BLACK,
		init            => snake_init,
		init_head_position   => create_vec2d(10,10),
		init_head_direction  => LEFT,
		init_body_length     => x"5",
		movement_direction   => movement_direction,
		move_head       => snake_move_head,
		move_tail       => snake_move_tail,
		done            => snake_done
	);

	prng_inst0 : prng
	port map (
		clk    => clk,
		res_n  => res_n,
		load_seed => load_seed,
		seed   => (others=>'0'),
		en     => '1',
		prdata => rnd_vec(0)
	);

	prng_inst1 : prng
	port map (
		clk    => clk,
		res_n  => res_n,
		load_seed => load_seed,
		seed   => x"42",
		en     => '1',
		prdata => rnd_vec(1)
	);

	synth_cntrl(0).high_time <= x"20";
	synth_cntrl(0).low_time <= x"20"; 
	synth_cntrl(0).play <= cntrl1.btn_up or cntrl1.btn_down;

	synth_cntrl(1).high_time <= x"10";
	synth_cntrl(1).low_time <= x"10"; 
	synth_cntrl(1).play <= cntrl1.btn_left or cntrl1.btn_right;
	
	process(all)
	begin
		game_state <= IDLE;
		if(cntrl1.btn_l = '1' and cntrl1.btn_r = '1') then
			game_state <= GAME_OVER;
		elsif(cntrl1.btn_l = '1') then
			game_state <= PAUSED;
		elsif(cntrl1.btn_r = '1') then
			game_state <= RUNNING;
		end if;
	end process;
	
end architecture;
