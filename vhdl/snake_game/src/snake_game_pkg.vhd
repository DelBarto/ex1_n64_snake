library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.n64_controller_pkg.all;
use work.audio_cntrl_pkg.all;

package snake_game_pkg is

	type snake_game_state_t is (IDLE, PAUSED, RUNNING, GAME_OVER);

	component snake_game is
		port (
			clk : in std_logic;
			res_n : in std_logic;
			instr_wr : out std_logic;
			instr : out std_logic_vector(3 downto 0);
			instr_data : out std_logic_vector(15 downto 0);
			instr_result : in std_logic_vector(15 downto 0);
			busy : in std_logic;
			vblank : in std_logic;
			cntrl1 : in n64_buttons_t;
			cntrl2 : in n64_buttons_t;
			synth_cntrl : out synth_cntrl_vec_t(0 to 1);
			game_state : out snake_game_state_t
		);
	end component;
end package;

