-- TASK 7: put your code here

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.ssd_cntrl_pkg.all;
use work.snake_game_pkg.all;
use work.n64_controller_pkg.all;

entity ssd_cntrl_tb is
end entity;

architecture bench of ssd_cntrl_tb is

	component ssd_cntrl is
		-- generic(
		-- 	NBR_CLK_CYCLES : integer := 4
		-- );
		port (
			clk : in std_logic;
			res_n : in std_logic;

			game_state : IN snake_game_state_t;
			--game_state : IN GAME_STATE_T;
			n64_cntrl : IN N64_BUTTONS_T;

			hex0 : OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
			hex1 : OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
			hex2 : OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
			hex3 : OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
			hex4 : OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
			hex5 : OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
			hex6 : OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
			hex7 : OUT STD_LOGIC_VECTOR(6 DOWNTO 0)
		);
	end component;

	signal clk : std_logic;
	signal res_n : std_logic;
	signal game_state : snake_game_state_t;
	signal n64_cntrl : N64_BUTTONS_T;

	constant CLK_PERIOD : time := 20 ns;
	signal stop_clock : boolean := false;
begin

	uut : ssd_cntrl
		port map (
			clk      	=> clk,
			res_n    	=> res_n,
			game_state	=> game_state,
			n64_cntrl	=> n64_cntrl
		);

	stimulus : process
	begin
		report "begin simulation";

		res_n <= '0';
		wait for 1.5*CLK_PERIOD;
		res_n <= '1';
		n64_cntrl <= N64_BUTTONS_RESET_VALUE;
		game_state <= IDLE;
		wait until falling_edge(clk);

		n64_cntrl.as_x <= std_logic_vector(to_signed(100 + (1526618 mod 27), 8));
		n64_cntrl.as_y <= std_logic_vector(to_signed(-(1526618 mod 32), 8));

		wait for 10*CLK_PERIOD;

		n64_cntrl.btn_start <= '1';
		wait for 50*CLK_PERIOD;

		n64_cntrl.btn_start <= '0';
		game_state <= RUNNING;
		wait for 50*CLK_PERIOD;

		game_state <= IDLE;
		wait for 50*CLK_PERIOD;

		game_state <= RUNNING;
		wait for 50*CLK_PERIOD;

		game_state <= IDLE;
		wait for 50*CLK_PERIOD;

		n64_cntrl.btn_start <= '1';
		n64_cntrl.btn_a <= '1';

		wait for 50*CLK_PERIOD;




		report "simulation Done";
		stop_clock <= true;

		wait;
	end process;

	generate_clk : process
	begin
		while not stop_clock loop
			clk <= '0', '1' after CLK_PERIOD / 2;
			wait for CLK_PERIOD;
		end loop;
		wait;
	end process;

end architecture;
