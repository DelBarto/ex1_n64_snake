# TASK 7

# arguments to the compiler
VCOM_ARGS=-2008 -work work -suppress 1236

# additional arguments to vsim
VSIM_ARGS=-msgmode both

# list of VHDL input files here
VHDL_FILES = \
	../synchronizer/src/sync_pkg.vhd\
	../synchronizer/src/sync.vhd\
	../audio_cntrl/src/audio_cntrl_pkg.vhd\
	../audio_cntrl/src/audio_cntrl_2s.vhd\
	../audio_cntrl/src/audio_cntrl_top.vho\
	../math/src/math_pkg.vhd\
	../n64_controller/src/n64_controller_pkg.vhd\
	../n64_controller/src/n64_controller.vhd\
	../prng/src/prng_pkg.vhd\
	../prng/src/prng.vho\
	../textmode_controller/src/font_rom_pkg.vhd\
	../textmode_controller/src/ltm/display_controller_pkg.vhd\
	../textmode_controller/src/ltm/display_controller.vhd\
	../textmode_controller/src/textmode_controller_pkg.vhd\
	../snake/src/snake_pkg.vhd\
	../uart_n64_bridge/src/uart_n64_bridge_pkg.vhd\
	../textmode_controller/src/textmode_controller.vhd\
	../snake_game/src/snake_game_pkg.vhd\
	../snake_game/src/snake_game.vhd\
	../textmode_controller/src/cursor_controller.vhd\
	../textmode_controller/src/textmode_controller_fsm.vhd\
	../textmode_controller/src/video_ram.vhd\
	../ssd_cntrl/src/ssd_cntrl_pkg.vhd\
	../ssd_cntrl/src/ssd_cntrl.vhd\
	../textmode_controller/src/font_rom.vhd\
	../snake/src/snake.vhd

TB_FILES = \
	../ssd_cntrl/tb/ssd_tb.vhd

TIME_RESOLUTION = 1ps
TB = ssd_cntrl_tb

# For the simulation time -all can also be selected. Questa then simulates until no more singal changes occour.
SIM_TIME = -all
WAVE_FILE = ./wave.do

compile: log

log: $(VHDL_FILES) $(TB_FILES)
	rm -f log
	vlib work | tee log
	for i in $(VHDL_FILES); do \
		vcom $(VCOM_ARGS) $$i | tee -a log;\
	done;
	for i in $(TB_FILES); do \
		vcom $(VCOM_ARGS) $$i | tee -a log;\
	done;
	@echo "--------------------------------------------------------------"
	@echo "--              Error and Warning Summary                   --"
	@echo "--------------------------------------------------------------"
	@cat log | grep 'Warning\|Error'
	@if [[ $$(grep "Error:" -m 1 log) ]]; then \
		echo "Compilation had errors!" \
		exit 1; \
	fi;

list_sources:
	@for i in $(VHDL_FILES) $(TB_FILES); do \
		echo $$i;\
	done;

sim: compile
	vsim -do "vsim $(TB) -t $(TIME_RESOLUTION) $(VSIM_ARGS); do $(WAVE_FILE);run $(SIM_TIME)"

sim_k: compile
	vsim -c -do "vsim $(TB) -t $(TIME_RESOLUTION) $(VSIM_ARGS); run -all;quit"

clean:
	rm -f transcript
	rm -f vsim.wlf
	rm -f log
	rm -fr work

.PHONY: clean
.PHONY: compile
.PHONY: sim
.PHONY: sim_k
