-- TASK 2: put your code here, Remember: Thou shalt not use the ALL keyword for sensitivity lists (in this file)


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.ssd_cntrl_pkg.all;
use work.snake_game_pkg.all;
use work.n64_controller_pkg.all;

----------------------------------------------------------------
--                           ENTITY                           --
----------------------------------------------------------------

entity ssd_cntrl is
	-- generic( -- TASK 8: BONUS TASK
	-- 	NBR_CLK_CYCLES : integer
	-- );
	port (
		clk : in std_logic;
		res_n : in std_logic;

		game_state : IN snake_game_state_t;
		--game_state : IN GAME_STATE_T;
		n64_cntrl : IN N64_BUTTONS_T;

		hex0 : OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
		hex1 : OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
		hex2 : OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
		hex3 : OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
		hex4 : OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
		hex5 : OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
		hex6 : OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
		hex7 : OUT STD_LOGIC_VECTOR(6 DOWNTO 0)
	);
end entity;




----------------------------------------------------------------------
--                           ARCHITECTURE                           --
----------------------------------------------------------------------

architecture beh of ssd_cntrl is
	type CONV_STATE is (SNAPSHOT,
						DISP_HEX,
						POS_VAL,
						NEG_VAL,
						CNT_HUN_DIGIT,
						CNT_TEN_DIGIT,
						ONES_DIGIT);

	function ana_to_hex(ana : STD_LOGIC_VECTOR(3 downto 0))
				return STD_LOGIC_VECTOR is
		variable hex : STD_LOGIC_VECTOR(6 downto 0) := "1111111";
	begin
		case ana is
			when "0000" => hex := "1000000";   -- hex 0
			when "0001" => hex := "1111001";   -- hex 1
			when "0010" => hex := "0100100";   -- hex 2
			when "0011" => hex := "0110000";   -- hex 3
			when "0100" => hex := "0011001";   -- hex 4
			when "0101" => hex := "0010010";   -- hex 5
			when "0110" => hex := "0000010";   -- hex 6
			when "0111" => hex := "1111000";   -- hex 7
			when "1000" => hex := "0000000";   -- hex 8
			when "1001" => hex := "0010000";   -- hex 9
			when "1010" => hex := "0001000";   -- hex a
			when "1011" => hex := "0000011";   -- hex b
			when "1100" => hex := "0100111";   -- hex c
			when "1101" => hex := "0100001";   -- hex d
			when "1110" => hex := "0000110";   -- hex f
			when "1111" => hex := "0001110";   -- hex f
			when others => hex := "1111111";   -- hex -
		end case;
		return hex;
	end ana_to_hex;


	signal state 			: CONV_STATE := SNAPSHOT;
	signal next_state 		: CONV_STATE := SNAPSHOT;
	signal ax_val 			: integer := 0;
	signal ax_val_next		: integer := 0;
	signal hdig_cnt			: integer := 0;
	signal hdig_cnt_next	: integer := 0;
	signal tdig_cnt			: integer := 0;
	signal tdig_cnt_next	: integer := 0;
	signal odig_cnt			: integer := 0;
	signal odig_cnt_next	: integer := 0;
	signal sign_val			: std_logic := '0';
	signal sign_val_next	: std_logic := '0';
	signal hex0_next : STD_LOGIC_VECTOR(6 DOWNTO 0);
	signal hex1_next : STD_LOGIC_VECTOR(6 DOWNTO 0);
	signal hex2_next : STD_LOGIC_VECTOR(6 DOWNTO 0);
	signal hex3_next : STD_LOGIC_VECTOR(6 DOWNTO 0);


	-- signal hex6_out : STD_LOGIC_VECTOR(6 DOWNTO 0);
	-- signal hex7_out : STD_LOGIC_VECTOR(6 DOWNTO 0);
	-- signal hex6_next : STD_LOGIC_VECTOR(6 DOWNTO 0);
	-- signal hex7_next : STD_LOGIC_VECTOR(6 DOWNTO 0);
	-- signal hex6_running_next : STD_LOGIC_VECTOR(6 DOWNTO 0);
	-- signal hex7_running_next : STD_LOGIC_VECTOR(6 DOWNTO 0);

begin
	hex4 <= "1111111";  -- off
	hex5 <= "1111111";  -- off

	task_2: process(game_state)
					-- , n64_cntrl)
	begin
		-- hex0 <= ana_to_hex(n64_cntrl.as_x(3 downto 0));
		-- hex1 <= ana_to_hex(n64_cntrl.as_x(7 downto 4));
		-- hex2 <= ana_to_hex(n64_cntrl.as_y(3 downto 0));
		-- hex3 <= ana_to_hex(n64_cntrl.as_y(7 downto 4));
		case game_state is
			when IDLE =>
				hex6(6 downto 0) <= "0111111";
				hex7(6 downto 0) <= "0111111";
			when RUNNING =>
				hex6(6 downto 0) <= "1111100";
				hex7(6 downto 0) <= "1100111";
			when PAUSED =>
				hex6(6 downto 0) <= "0001000";
				hex7(6 downto 0) <= "0001100";
			when GAME_OVER =>
				hex6(6 downto 0) <= "1000000";
				hex7(6 downto 0) <= "1000010";
		end case;
	end process task_2;

	-- task_2: process(game_state)
	-- -- , n64_cntrl)
	-- begin
	-- 	-- hex0 <= ana_to_hex(n64_cntrl.as_x(3 downto 0));
	-- 	-- hex1 <= ana_to_hex(n64_cntrl.as_x(7 downto 4));
	-- 	-- hex2 <= ana_to_hex(n64_cntrl.as_y(3 downto 0));
	-- 	-- hex3 <= ana_to_hex(n64_cntrl.as_y(7 downto 4));
	-- 	case game_state is
	-- 		when IDLE =>
	-- 		hex6_next(6 downto 0) <= "0111111";
	-- 		hex7_next(6 downto 0) <= "0111111";
	-- 		when RUNNING =>
	-- 		hex6_next(6 downto 0) <= "1111100";
	-- 		hex7_next(6 downto 0) <= "1100111";
	-- 		when PAUSED =>
	-- 		hex6_next(6 downto 0) <= "0001000";
	-- 		hex7_next(6 downto 0) <= "0001100";
	-- 		when GAME_OVER =>
	-- 		hex6_next(6 downto 0) <= "1000000";
	-- 		hex7_next(6 downto 0) <= "1000010";
	-- 	end case;
	-- end process task_2;

	-- -- TASK 7: BONUS TASK
	-- t7_bonus:process(clk, game_state)
	-- 	type RUNNING_ANIMATION is (DL, DD, RD, RR, UR, UU, LU, LL);
	-- 	variable clk_cyc_cnt : integer := 0;
	-- 	variable ani : RUNNING_ANIMATION := DL;
	-- begin
	-- 	if (game_state = RUNNING) then
	-- 		if (rising_edge(clk)) then
	-- 			if ((clk_cyc_cnt mod NBR_CLK_CYCLES) = 0) then
	-- 				clk_cyc_cnt := 0;
	-- 				case ani is
	-- 					when DL => -- ----------------------------
	-- 						hex6_running_next(6 downto 0) <= "1111111";
	-- 						hex7_running_next(6 downto 0) <= "1100111";
	-- 						ani := DD;
	--
	-- 					when DD => -- ----------------------------
	-- 						hex6_running_next(6 downto 0) <= "1110111";
	-- 						hex7_running_next(6 downto 0) <= "1110111";
	-- 						ani := RD;
	--
	-- 					when RD => -- ----------------------------
	-- 						hex6_running_next(6 downto 0) <= "1110011";
	-- 						hex7_running_next(6 downto 0) <= "1111111";
	-- 						ani := RR;
	--
	-- 					when RR => -- ----------------------------
	-- 						hex6_running_next(6 downto 0) <= "1111001";
	-- 						hex7_running_next(6 downto 0) <= "1111111";
	-- 						ani := UR;
	--
	-- 					when UR => -- ----------------------------
	-- 						hex6_running_next(6 downto 0) <= "1111100";
	-- 						hex7_running_next(6 downto 0) <= "1111111";
	-- 						ani := UU;
	--
	-- 					when UU => -- ----------------------------
	-- 						hex6_running_next(6 downto 0) <= "1111110";
	-- 						hex7_running_next(6 downto 0) <= "1111110";
	-- 						ani := LU;
	--
	-- 					when LU => -- ----------------------------
	-- 						hex6_running_next(6 downto 0) <= "1111111";
	-- 						hex7_running_next(6 downto 0) <= "1011110";
	-- 						ani := LL;
	--
	-- 					when LL => -- ----------------------------
	-- 						hex6_running_next(6 downto 0) <= "1111111";
	-- 						hex7_running_next(6 downto 0) <= "1001111";
	-- 						ani := DL;
	-- 				end case;
	-- 			else
	-- 				clk_cyc_cnt := clk_cyc_cnt + 1;
	-- 			end if;
	-- 		end if;
	-- 	else
	-- 		ani := DL;
	-- 		clk_cyc_cnt := 0;
	-- 	end if;
	-- end process t7_bonus;
	--
	--
	-- t7_bonus_sync:process(clk, game_state)
	-- begin
	-- 	if rising_edge(clk) then
	-- 		if res_n = '0' then
	-- 			hex6_out <= (others => '1');
	-- 			hex7_out <= (others => '1');
	-- 		else
	-- 			if game_state = RUNNING then
	-- 				hex6_out <= hex6_running_next;
	-- 				hex7_out <= hex7_running_next;
	-- 			else
	-- 				hex6_out <= hex6_next;
	-- 				hex7_out <= hex7_next;
	-- 			end if;
	-- 		end if;
	-- 	end if;
	-- end process;
	--
	--
	-- t7_bonus_out : process(hex6_out, hex7_out)
	-- begin
	-- 	hex6 <= hex6_out;
	-- 	hex7 <= hex7_out;
	-- end process;



-- --------------------------------------------------------- --
--						NEXT STATE LOGIC					 --
-- --------------------------------------------------------- --
-- TASK 7: state machine to display the analog values of the x-axis and y-axis
	t7_next_state_logic:process(state, ax_val, sign_val, n64_cntrl,
								hdig_cnt, tdig_cnt, odig_cnt)
	begin
		next_state <= state;
		ax_val_next <= ax_val;
		sign_val_next <= sign_val;
		hdig_cnt_next <= hdig_cnt;
		tdig_cnt_next <= tdig_cnt;
		odig_cnt_next <= odig_cnt;

		case state is
			when SNAPSHOT			=> -- ----------------------------
				if n64_cntrl.btn_start = '1'
					and n64_cntrl.btn_a = '0'
					and n64_cntrl.as_x(7) = '0' then
						ax_val_next		<= to_integer(signed(n64_cntrl.as_x));
						next_state <= POS_VAL;
				elsif n64_cntrl.btn_start = '1'
					and n64_cntrl.btn_a = '0'
					and n64_cntrl.as_x(7) = '1' then
						ax_val_next		<= to_integer(signed(n64_cntrl.as_x));
						next_state <= NEG_VAL;
				elsif n64_cntrl.btn_start = '1'
					and n64_cntrl.btn_a = '1'
					and n64_cntrl.as_y(7) = '0' then
						ax_val_next		 <= to_integer(signed(n64_cntrl.as_y));
						next_state <= POS_VAL;
				elsif n64_cntrl.btn_start = '1'
					and n64_cntrl.btn_a = '1'
					and n64_cntrl.as_y(7) = '1' then
						ax_val_next		<= to_integer(signed(n64_cntrl.as_y));
						next_state <= NEG_VAL;
				else
					next_state <= DISP_HEX;
				end if;

			when DISP_HEX			=> -- ----------------------------
				next_state <= SNAPSHOT;

			when POS_VAL			=> -- ----------------------------
				sign_val_next <= '1';
				next_state <= CNT_HUN_DIGIT;

			when NEG_VAL			=> -- ----------------------------
				ax_val_next <= -ax_val;
				sign_val_next <= '0';
				next_state <= CNT_HUN_DIGIT;

			when CNT_HUN_DIGIT		=> -- ----------------------------
				if ax_val < 100 then
					next_state <= CNT_TEN_DIGIT;
				else
					hdig_cnt_next <= hdig_cnt + 1;
					ax_val_next <= ax_val - 100;
					next_state <= CNT_HUN_DIGIT;
				end if;

			when CNT_TEN_DIGIT		=> -- ----------------------------
				if ax_val < 10 then
					odig_cnt_next <= ax_val;
					next_state <= ONES_DIGIT;
				else
					tdig_cnt_next <= tdig_cnt + 1;
					ax_val_next <= ax_val - 10;
					next_state <= CNT_TEN_DIGIT;
				end if;

			when ONES_DIGIT			=> -- ----------------------------
				sign_val_next	<= '0';
				hdig_cnt_next 	<= 0;
				tdig_cnt_next 	<= 0;
				odig_cnt_next 	<= 0;
				ax_val_next <= 0;
				next_state <= SNAPSHOT;
		end case;
	end process;


-- --------------------------------------------------------- --
--							SYNC PROCESS					 --
-- --------------------------------------------------------- --
	t7_sync:process(clk, res_n)
	begin
		if rising_edge(clk) then
			if (res_n = '0') then -- reset signals
				state 		<= SNAPSHOT;
				sign_val	<= '0';
				hdig_cnt 	<= 0;
				tdig_cnt 	<= 0;
				odig_cnt 	<= 0;
				ax_val 		<= 0;
			else
				state 		<= next_state;
				ax_val 		<= ax_val_next;
				sign_val 	<= sign_val_next;
				hdig_cnt 	<= hdig_cnt_next;
				tdig_cnt 	<= tdig_cnt_next;
				odig_cnt 	<= odig_cnt_next;
				hex0 		<= hex0_next;
				hex1 		<= hex1_next;
				hex2 		<= hex2_next;
				hex3 		<= hex3_next;
			end if;
		end if;
	end process;


-- --------------------------------------------------------- --
--							OUTPUT LOGIC					 --
-- --------------------------------------------------------- --
	t7_output_logic:process(state, n64_cntrl, odig_cnt, tdig_cnt, hdig_cnt,
							sign_val, hex0, hex1, hex2, hex3)
	begin
		hex0_next <= hex0;
		hex1_next <= hex1;
		hex2_next <= hex2;
		hex3_next <= hex3;

		case state is
			when DISP_HEX			=> -- ----------------------------
				hex0_next <= ana_to_hex(n64_cntrl.as_x(3 downto 0));
				hex1_next <= ana_to_hex(n64_cntrl.as_x(7 downto 4));
				hex2_next <= ana_to_hex(n64_cntrl.as_y(3 downto 0));
				hex3_next <= ana_to_hex(n64_cntrl.as_y(7 downto 4));

			when ONES_DIGIT			=> -- ----------------------------
				hex0_next <= ana_to_hex(std_logic_vector(to_unsigned(odig_cnt, 4)));
				hex1_next <= ana_to_hex(std_logic_vector(to_unsigned(tdig_cnt, 4)));
				hex2_next <= ana_to_hex(std_logic_vector(to_unsigned(hdig_cnt, 4)));
				hex3_next <= sign_val & "111111";

			when others				=>

		end case;
	end process;

end architecture;
