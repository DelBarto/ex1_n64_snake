onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider colors
add wave -noupdate /ssd_cntrl_tb/clk
add wave -noupdate /ssd_cntrl_tb/res_n
add wave -noupdate /ssd_cntrl_tb/game_state
add wave -noupdate /ssd_cntrl_tb/n64_cntrl
add wave -noupdate -divider colors
add wave -noupdate /ssd_cntrl_tb/uut/state
add wave -noupdate /ssd_cntrl_tb/n64_cntrl.btn_start
add wave -noupdate /ssd_cntrl_tb/n64_cntrl.btn_a
add wave -noupdate /ssd_cntrl_tb/uut/ax_val
add wave -noupdate /ssd_cntrl_tb/uut/sign_val
add wave -noupdate /ssd_cntrl_tb/uut/hdig_cnt
add wave -noupdate /ssd_cntrl_tb/uut/tdig_cnt
add wave -noupdate /ssd_cntrl_tb/uut/odig_cnt
add wave -noupdate /ssd_cntrl_tb/uut/hex0
add wave -noupdate /ssd_cntrl_tb/uut/hex1
add wave -noupdate /ssd_cntrl_tb/uut/hex2
add wave -noupdate /ssd_cntrl_tb/uut/hex3
add wave -noupdate /ssd_cntrl_tb/uut/hex4
add wave -noupdate /ssd_cntrl_tb/uut/hex5
add wave -noupdate /ssd_cntrl_tb/uut/hex6
add wave -noupdate /ssd_cntrl_tb/uut/hex7
