
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.n64_controller_pkg.all;
use work.math_pkg.all;

entity uart_n64_bridge is
	generic (
		TX_TIMEOUT : integer := 500_000
	);
	port (
		clk : in std_logic;
		res_n : in std_logic; 
		
		button_state_in  : in n64_buttons_t;
		button_state_out : out n64_buttons_t;
		
		tx_data  : out std_logic_vector(7 downto 0);
		tx_wr    : out std_logic;
		tx_free  : in std_logic;
		
		rx_data  : in std_logic_vector(7 downto 0);
		rx_rd    : out std_logic;
		rx_empty : in std_logic
	);
end entity;

architecture arch of uart_n64_bridge is
	constant SYNC_PATTERN : std_logic_vector(7 downto 0) := x"ff";
begin
	n64_to_uart : block 
		type tx_state_t is (READ_INPUT, SEND_SYNC_PATTERN, SEND_BYTE_1, SEND_BYTE_2, SEND_BYTE_3, SEND_BYTE_4, TIMEOUT);
		signal tx_state : tx_state_t;
		signal timeout_cnt : std_logic_vector(log2c(TX_TIMEOUT+1)-1 downto 0);
		signal button_state : n64_buttons_t;
	begin
		
		sync : process(clk, res_n)
		begin
			if (res_n = '0') then
				tx_data <= (others=>'0');
				timeout_cnt <= (others=>'0');
				tx_wr <= '0';
				tx_state <= TIMEOUT;
				button_state <= (as_x => (others => '0'), as_y => (others => '0'), others=>'0');
			elsif (rising_edge(clk)) then
				tx_wr <= '0';
				case tx_state is
					when READ_INPUT =>
						button_state <= button_state_in;
						tx_state <= SEND_SYNC_PATTERN;
					
					when SEND_SYNC_PATTERN =>
						tx_data <= SYNC_PATTERN;
						if (tx_free = '1') then
							tx_wr <= '1';
							tx_state <= SEND_BYTE_1;
						end if;

					when SEND_BYTE_1 =>
						tx_data(0) <= button_state.btn_a;
						tx_data(1) <= button_state.btn_b;
						tx_data(2) <= button_state.btn_z;
						tx_data(3) <= button_state.btn_start;
						tx_data(4) <= button_state.btn_up;
						tx_data(5) <= button_state.btn_down;
						tx_data(6) <= button_state.btn_left;
						tx_data(7) <= button_state.btn_right;
						if (tx_free = '1') then
							tx_wr <= '1';
							tx_state <= SEND_BYTE_2;
						end if;
					
					when SEND_BYTE_2 =>
						tx_data <= (others=>'1');
						tx_data(2) <= button_state.btn_l;
						tx_data(3) <= button_state.btn_r;
						tx_data(4) <= button_state.btn_c_up;
						tx_data(5) <= button_state.btn_c_down;
						tx_data(6) <= button_state.btn_c_left;
						tx_data(7) <= button_state.btn_c_right;
						if (tx_free = '1') then
							tx_wr <= '1';
							tx_state <= SEND_BYTE_3; --TIMEOUT;
						end if;
					when SEND_BYTE_3 =>
						tx_data <= button_state.as_x;
						if (tx_free = '1') then
							tx_wr <= '1';
							tx_state <= SEND_BYTE_4;
						end if;
					when SEND_BYTE_4 =>
						tx_data <= button_state.as_y;
						if (tx_free = '1') then
							tx_wr <= '1';
							tx_state <= TIMEOUT;
						end if;		
					when TIMEOUT => 
						if (unsigned(timeout_cnt) = TX_TIMEOUT ) then
							timeout_cnt <= (others=>'0');
							tx_state <= READ_INPUT;
						else
							timeout_cnt <= std_logic_vector(unsigned(timeout_cnt)+1);
						end if;
				end case;
			end if;
		end process;
		
		
	end block;
	
	
	uart_to_n64 : block
		type rx_state_t is (IDLE, CHECK_SYNC_PATTERN, READ_BYTE_1, READ_BYTE_2, READ_BYTE_3, READ_BYTE_4);
		signal rx_state, rx_state_nxt : rx_state_t;
		signal byte_1, byte_2, byte_3, byte_1_nxt, byte_2_nxt, byte_3_nxt : std_logic_vector(7 downto 0);
		signal button_state_out_nxt : n64_buttons_t;
	begin
		sync : process(clk, res_n)
		begin
			if (res_n = '0') then
				rx_state <= IDLE;
				button_state_out <= (as_x => (others => '0'), as_y => (others => '0'), others=>'0');
				byte_1 <= (others=>'0');
				byte_2 <= (others=>'0');
				byte_3 <= (others=>'0');
			elsif (rising_edge(clk)) then
				byte_1 <= byte_1_nxt;
				byte_2 <= byte_2_nxt;
				byte_3 <= byte_3_nxt;
				rx_state <= rx_state_nxt;
				button_state_out <= button_state_out_nxt;
			end if;
		end process;
	
		next_state : process(all)
		begin
			byte_1_nxt <= byte_1;
			byte_2_nxt <= byte_2;
			byte_3_nxt <= byte_3;
			rx_state_nxt <= rx_state;
			button_state_out_nxt <= button_state_out;
		
			rx_rd <= '0';
			case rx_state is
				when IDLE =>
					if (rx_empty = '0') then
						rx_state_nxt <= CHECK_SYNC_PATTERN;
						rx_rd <= '1';
					end if;
				
				when CHECK_SYNC_PATTERN =>
					if(rx_data = SYNC_PATTERN) then
						if (rx_empty = '0') then
							rx_state_nxt <= READ_BYTE_1;
							rx_rd <= '1';
						end if;
					else 
						rx_state_nxt <= IDLE;
					end if;
					
				when READ_BYTE_1 =>
					byte_1_nxt <= rx_data;
					if (rx_empty = '0') then
						rx_state_nxt <= READ_BYTE_2;
						rx_rd <= '1';
					end if;
					
				when READ_BYTE_2 =>
					byte_2_nxt <= rx_data;
					if (rx_empty = '0') then
						rx_state_nxt <= READ_BYTE_3;
						rx_rd <= '1';
					end if;
				when READ_BYTE_3 =>
					byte_3_nxt <= rx_data;
					if (rx_empty = '0') then
						rx_state_nxt <= READ_BYTE_4;
						rx_rd <= '1';
					end if;
				when READ_BYTE_4 =>
					button_state_out_nxt.btn_a      <= byte_1(0);
					button_state_out_nxt.btn_b      <= byte_1(1);
					button_state_out_nxt.btn_z      <= byte_1(2);
					button_state_out_nxt.btn_start  <= byte_1(3);
					button_state_out_nxt.btn_up     <= byte_1(4);
					button_state_out_nxt.btn_down   <= byte_1(5);
					button_state_out_nxt.btn_left   <= byte_1(6);
					button_state_out_nxt.btn_right  <= byte_1(7);
					
					button_state_out_nxt.btn_l      <= byte_2(2);
					button_state_out_nxt.btn_r      <= byte_2(3);
					button_state_out_nxt.btn_c_up   <= byte_2(4);
					button_state_out_nxt.btn_c_down <= byte_2(5);
					button_state_out_nxt.btn_c_left <= byte_2(6);
					button_state_out_nxt.btn_c_right<= byte_2(7);
					
					button_state_out_nxt.as_x <= byte_3;
					
					button_state_out_nxt.as_y <= rx_data;

					rx_state_nxt <= IDLE;
			end case;
	
		end process;
	
	end block;
	
	
end architecture;

