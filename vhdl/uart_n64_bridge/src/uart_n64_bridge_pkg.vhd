library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


use work.n64_controller_pkg.all;

package uart_n64_bridge_pkg is

	component uart_n64_bridge is
		generic (
			TX_TIMEOUT : integer := 500_000
		);
		port (
			clk : in std_logic;
			res_n : in std_logic;
			button_state_in : in n64_buttons_t;
			button_state_out : out n64_buttons_t;
			tx_data : out std_logic_vector(7 downto 0);
			tx_wr : out std_logic;
			tx_free : in std_logic;
			rx_data : in std_logic_vector(7 downto 0);
			rx_rd : out std_logic;
			rx_empty : in std_logic
		);
	end component;
end package;

