#!/bin/python3

from docopt import docopt
import os
import subprocess
import curses
from enum import IntEnum, Enum


usage_msg = """
Usage:
  remote.py (-k | -s | -n ) [-b B | -a] [<VALUE>]
  remote.py (-g | -r | -x) [-a]
  remote.py (-i | -p SOF)
  remote.py -h

Options:
  -h      Show this help screen
  -i      Interactive Mode
  -k      Access the push buttons (4 bit)
  -s      Access the switches (18 bit)
  -n      Access the N64 contoller (32 bit)
  -g      Access the green LEDs (9 bit)
  -r      Access the red LEDs (18 bit)
  -x      Access the hex display (8 x 7 bit)
  -b B    Access individual bits or bit ranges (a:b)
  -a      Use ASCII art to present the results
  -p SOF  Download SOF file to the FPGA board
"""

ADDR_KEYS = 0
ADDR_SWITCHES = 1
ADDR_N64_CONTROLLER = 2
ADDR_LEDR = 3
ADDR_LEDG = 4
ADDR_HEX0_3 = 5
ADDR_HEX4_7 = 6

class RemoteBridge:
	
	def __init__(self):
		subprocess.run(["killall", "nios2-terminal"], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
		self.proc = subprocess.Popen(["nios2-terminal", "-q"], stdin=subprocess.PIPE, stdout=subprocess.PIPE)
	
	def ReadAddress(self, address):
		self.proc.stdin.write(b"r\n")
		self.proc.stdin.write(str(address).encode("utf-8")+ b"\n")
		self.proc.stdin.flush()
		response = self.proc.stdout.readline().decode("utf-8").rstrip()
		if(response == "error"):
			raise Exception("Error reading value")
		else:
			return int(response, 0)
	
	def WriteAddress(self, address, value):
		self.proc.stdin.write(b"w\n")
		self.proc.stdin.write(str(address).encode("utf-8") + b"\n")
		self.proc.stdin.write(str(value).encode("utf-8") + b"\n")
		self.proc.stdin.flush()
		response = self.proc.stdout.readline().decode("utf-8").rstrip()
		if(response != "ok"):
			raise Exception("Error writing value")
	
	def Close(self):
		self.proc.stdin.write(b"e\n")


class InputDevice(IntEnum):
	KEYS = 0
	SWITCHES = 1
	N64_CONTROLLER = 2
	
	def GetBaseAddress(self):
		if(self == InputDevice.KEYS):
			return ADDR_KEYS
		elif(self == InputDevice.SWITCHES):
			return ADDR_SWITCHES
		elif(self == InputDevice.N64_CONTROLLER):
			return ADDR_N64_CONTROLLER

	def DrawDevice(self, state):
		if(self == InputDevice.KEYS):
			return InputDevice.DrawButtons(state)
		elif(self == InputDevice.SWITCHES):
			return InputDevice.DrawSwitches(state)
		elif(self == InputDevice.N64_CONTROLLER):
			return InputDevice.DrawController(state)

	@staticmethod
	def DrawSwitches(value):
		#draw the numbers
		header_line = ""
		line1 = ""
		line2 = ""
		for i in reversed(range(0,18)):
			header_line += str(i).rjust(3, " ") + " "
			if((value >> i) & 0x01 == 1):
				line1 += "│[]│"
				line2 += "│  │"
			else:
				line1 += "│  │"
				line2 += "│[]│"
			
		output = header_line + "\n"
		output += "┌──┐"*18 + "\n"
		output += line1 + "\n"
		output += line2 + "\n"
		output += "└──┘"*18 + "\n"
		return output

	@staticmethod
	def DrawButtons(value):
		#draw the numbers
		output = ""
		header_line = ""
		line = ""
		for i in reversed(range(0,4)):
			header_line += str(i).rjust(3, " ") + " "
			if((value >> i) & 0x01 == 0):
				line += "│()│"
			else:
				line += "│  │"
			
		output += header_line + "\n"
		output += "┌──┐"*4 + "\n"
		output += line + "\n"
		output += "└──┘"*4 + "\n"
		return output
	
	@staticmethod
	def DrawController(state):
		n64_controller_image = r"""
               _,.-------.,_
      ________|             |________
   .-' [L]    |     [Z]     |    [R] '-.
 .'                                     '.  
/    [↑]                           [Λ]    \ 
| [←]   [→]                     [<]   [>] | 
\    [↓]            [S]      [B]   [V]    / 
|`.                            [A]      ,'| 
|  `-.________     .---.     ________,-'  | 
|      |      `.  | (-) |  ,'      |      | 
|      |        \  '___'  /        |      | 
|      /        |         |        \      | 
\     /         | x[xxxx] |         \     / 
 `._,'          \ y[yyyy] /          `._,'  
                 \       /
                  `.___,'
"""
		def sign_extend(value, bits):
			sign_bit = 1 << (bits - 1)
			return (value & (sign_bit - 1)) - (value & sign_bit)

		def coord_to_str(c):
			c_str = ""
			if (c > 0):
				c_str += "+"
			elif (c < 0):
				c_str += "-"
			else:
				c_str += " "
			
			c_str += str(abs(c)).rjust(3, " ")
			return c_str

		output = n64_controller_image
		r = [
		(bool(state >> 0 & 1), "A"),
		(bool(state >> 1 & 1), "B"), 
		(bool(state >> 2 & 1), "Z"),
		(bool(state >> 3 & 1), "S"),
		(bool(state >> 4 & 1), "↑"),
		(bool(state >> 5 & 1), "↓"),
		(bool(state >> 6 & 1), "←"),
		(bool(state >> 7 & 1), "→"),
		(bool(state >> 10 & 1), "L"),
		(bool(state >> 11 & 1), "R"),
		(bool(state >> 12 & 1), "Λ"),
		(bool(state >> 13 & 1), "V"),
		(bool(state >> 14 & 1), "<"),
		(bool(state >> 15 & 1), ">")
		]
		for x in r:
			if(not x[0]):
				output = output.replace("["+x[1]+"]", " " + x[1] + " ")
		
		as_x = sign_extend(state >> 16 & 0xff, 8)
		as_y = sign_extend(state >> 24 & 0xff, 8)

		output = output.replace("xxxx", coord_to_str(as_x))
		output = output.replace("yyyy", coord_to_str(as_y))
		
		return output





def Draw7SegmentChars(data):
	hex_template = """\
  0000  
 5    1 
 5    1 
  6666  
 4    2 
 4    2 
  3333  \
"""
	def DrawSingleChar(c):
		output = hex_template
		for i in [0,6,3]:
			if ((c>>i) & 0x1):
				output = output.replace(str(i), ".") #┄
			else:
				output = output.replace(str(i), "━")
				
		for i in [1,2,4,5]:
			if ((c>>i) & 0x1):
				output = output.replace(str(i), ".") #┊
			else:
				output = output.replace(str(i), "┃")
		return output

	output_chars = [ DrawSingleChar(c).split("\n") for c in data ]
	output_str = ""
	
	for i in range(len(output_chars[0])):
		for j in range(len(data)):
			output_str += output_chars[j][i]
		
		if(i!=len(output_chars[0])-1):
			output_str += '\n'
	
	output_str += "\n"
	
	for i in range(0,len(data)):
		output_str += ("  HEX" + str(i) + " ").ljust(len(output_chars[0][0])," ")
	
	
	
	return output_str


# 0   1
#┌─┐ ┌─┐
#│●│ │○│  
#└─┘ └─┘

def DrawLEDs(value, length):
	header_line = ""
	line = ""
	for i in reversed(range(0,length)):
		header_line += str(i).rjust(2, " ") + " "
		if((value >> i) & 0x01 == 1):
			line += "│●│"
		else:
			line += "│○│"
		
	output = ""
	output += header_line + "\n"
	output += "┌─┐"*length + "\n"
	output += line + "\n"
	output += "└─┘"*length
	return output



def ReadHexValues(remote):
	hex0_3 = remote.ReadAddress(ADDR_HEX0_3)
	hex4_7 = remote.ReadAddress(ADDR_HEX4_7)
	
	hex_values = hex0_3 + (hex4_7<<32) 
	hex_values_list = []
	
	for i in range(0,8):
		hex_values_list.append(hex_values >> 8*i & 0x7f)

	return hex_values_list

switches_keymap = {
	ord('0'): 0,
	ord('1'): 1,
	ord('2'): 2,
	ord('3'): 3,
	ord('4'): 4,
	ord('5'): 5,
	ord('6'): 6,
	ord('7'): 7,
	ord('8'): 8,
	ord('9'): 9,
	ord('q'): 10,
	ord('w'): 11,
	ord('e'): 12,
	ord('r'): 13,
	ord('t'): 14,
	ord('y'): 15,
	ord('u'): 16,
	ord('i'): 17,
}

keys_keymap = {
	ord('0'): 0,
	ord('1'): 1,
	ord('2'): 2,
	ord('3'): 3
}

n64_controller_keymap = {
	ord('a'): 0,
	ord('b'): 1,
	ord('z'): 2,
	ord('s'): 3,
	curses.KEY_UP : 4,
	curses.KEY_DOWN : 5,
	curses.KEY_LEFT : 6,
	curses.KEY_RIGHT : 7,
	ord('l'): 10,
	ord('r'): 11,
	ord('8'): 12,
	ord('2'): 13,
	ord('4'): 14,
	ord('6'): 15
}

n64_controller_as_keymap = {
	ord('['): ("x", -1),
	ord(']'): ("x", 1),
	ord('{'): ("y", -1),
	ord('}'): ("y", 1)
}

def GetKeyString(key):
	if (key > 255):
		special_keys = {
			curses.KEY_UP    : "↑",
			curses.KEY_DOWN  : "↓",
			curses.KEY_LEFT  : "←",
			curses.KEY_RIGHT : "→",
			curses.KEY_END   : "End",
			curses.KEY_NPAGE : "Page Up",
			curses.KEY_PPAGE : "Page Down"
		}
		return special_keys[key]
	else:
		return chr(key)

class ViewMode(Enum):
	Input  = 0
	Output = 1
	Help   = 2


def InteractiveModePrintHelpScreen(stdscr):

	def GetKeyToValue(d, value):
		return list(d.keys())[list(d.values()).index(value)]

	help_text = """
Commands: 
 > page up/down:  Cycle through the available input devices.
 > v:  Switch between the views for input and output devices.
 > m:  Switch between between the persistent and non-persistent input mode.
       In the persistent input mode pressed keys/switches must be reset manually
       by pressing the same key again. In the non-persistent mode keys are reset
       automatically after some short time.
 > h:  Show/hide this help screen.
"""

	stdscr.addstr("Key mappings for inputs devices:\n")

	stdscr.addstr(" > Switches: SW18,...,SW0: ")
	for i in reversed(range(0,18)):
		stdscr.addstr(GetKeyString(list(switches_keymap.keys())[list(switches_keymap.values()).index(i)]))
		if(i!=0):
			stdscr.addstr(", ")

	stdscr.addstr("\n > Keys: KEY3,...,KEY0: ")
	for i in reversed(range(0,4)):
		stdscr.addstr(GetKeyString(list(keys_keymap.keys())[list(keys_keymap.values()).index(i)]))
		if(i!=0):
			stdscr.addstr(", ")


	stdscr.addstr("\n > N64 Controler:\n")
	stdscr.addstr("    A, B, Start, L, R, Z :")
	mapping = [
		GetKeyString(GetKeyToValue(n64_controller_keymap, i)) 
		for i in [0,1,3,10,11,2]
	]
	stdscr.addstr(", ".join(mapping))
	stdscr.addstr("\n    Arrow Buttons (up, down, left, right): ")
	mapping = [
		GetKeyString(GetKeyToValue(n64_controller_keymap, i)) 
		for i in range(4,8)
	]
	stdscr.addstr(", ".join(mapping))
	stdscr.addstr("\n    C Arrow Buttons (up, down, left, right): ")
	mapping = [
		GetKeyString(GetKeyToValue(n64_controller_keymap, i)) 
		for i in range(12,16)
	]
	stdscr.addstr(", ".join(mapping))
	stdscr.addstr("\n    Analog Stick (-x, +x, -y, +y): ")
	mapping = [
		GetKeyString(GetKeyToValue(n64_controller_as_keymap, i)) 
		for i in [("x",-1), ("x",1), ("y",-1), ("y",+1)]
	]
	stdscr.addstr(", ".join(mapping))


	stdscr.addstr("\n" + help_text)

	stdscr.addstr("\n\nExit with ctrl +c or end, press h to retrun to previous screen")

def InteractiveModePrintInputDevices(stdscr, cur_state, selected_device, persistent_mode):
	def print_header(text, selected):
		if (selected):
			stdscr.addstr("[[ " + text + " ]]")
			if (not persistent_mode):
				stdscr.addstr(" >> non-persistant mode <<")
			stdscr.addstr("\n")
		else:
			stdscr.addstr(text + "\n")
			
	print_header(
		"Switches: " + hex(cur_state[InputDevice.SWITCHES]),
		selected_device == InputDevice.SWITCHES
	)
	stdscr.addstr(InputDevice.SWITCHES.DrawDevice(cur_state[InputDevice.SWITCHES]))
	stdscr.addstr("\n")

	print_header(
		"Keys: " + hex(cur_state[InputDevice.KEYS]),
		selected_device == InputDevice.KEYS
	)
	stdscr.addstr(InputDevice.KEYS.DrawDevice(cur_state[InputDevice.KEYS]))
	stdscr.addstr("\n")

	print_header(
		"N64 Contoller: " + hex(cur_state[InputDevice.N64_CONTROLLER]),
		selected_device == InputDevice.N64_CONTROLLER
	)
	stdscr.addstr(InputDevice.N64_CONTROLLER.DrawDevice(cur_state[InputDevice.N64_CONTROLLER]))

def InteractiveModePrintOutputDevices(stdscr, remote):
	ledr = remote.ReadAddress(ADDR_LEDR)
	stdscr.addstr("\nRed LEDs (ledr): " + hex(ledr) + "\n")
	stdscr.addstr(DrawLEDs(ledr, 18))

	ledg = remote.ReadAddress(ADDR_LEDG)
	stdscr.addstr("\n\nGreen LEDs (ledg): " + hex(ledg) + "\n")
	stdscr.addstr(DrawLEDs(ledg, 9))
	
	hex_values_list = ReadHexValues(remote)
	hex_values_list.reverse()
	stdscr.addstr("\n\nSeven Segment Display (hex{7-0}): ")
	for h in hex_values_list:
		stdscr.addstr('0x{0:0{1}X}'.format(h,2)+" ")
	#stdscr.addstr("\n"+Draw7SegmentChars(hex_values_list))
	stdscr.addstr("\n")
	for c in Draw7SegmentChars(hex_values_list):
		if (c.isalnum() or c in [" " ,"."]):
			stdscr.addstr(c)
		else:
			stdscr.addstr(c, curses.A_BOLD | curses.color_pair(1))


def InteractiveMode(stdscr):
	remote = RemoteBridge()
	curses.start_color()
	curses.init_pair(1, curses.COLOR_RED, curses.COLOR_BLACK)
	stdscr.clear()
	stdscr.refresh()
	stdscr.nodelay(True) # non-blocking getch
	
	#check the bounds of the window
	height, width = stdscr.getmaxyx()
	if(height < 34):
		raise Exception("Error: Increase the height of your terminal! (min. 34 lines)")
	if(width < 80):
		raise Exception("Error: Increase the width of your terminal! (min. 80 columns)")
	
	stop = False
	view_mode = ViewMode.Input
	selected_device = InputDevice.SWITCHES
	sticky_mode = True
	
	cur_state = {
		InputDevice.SWITCHES : remote.ReadAddress(ADDR_SWITCHES),
		InputDevice.KEYS :remote.ReadAddress(ADDR_KEYS),
		InputDevice.N64_CONTROLLER : remote.ReadAddress(ADDR_N64_CONTROLLER)
	}
	
	prev_states = [cur_state[selected_device]] * 3

	while (not stop):
		
		if(view_mode == ViewMode.Input or view_mode == ViewMode.Output):
			skip_input_processing = False
			
			stdscr.clear()
			if(view_mode == ViewMode.Input):
				curses.napms(50)
				InteractiveModePrintInputDevices(stdscr, cur_state, selected_device, sticky_mode)
			else:
				InteractiveModePrintOutputDevices(stdscr, remote)
			stdscr.addstr("\n\nExit with Ctrl+C or End, press h for the help screen")
			stdscr.refresh()
			
			if(sticky_mode):
				new_state = cur_state[selected_device]
			else:
				if (selected_device == InputDevice.KEYS):
					new_state = 0xf
				else:
					new_state = 0x0
				
			while(True):
				c = stdscr.getch()
				if (c == curses.KEY_END or c == 27):
					stop = True
					break
				elif (c == -1):
					break
				elif (c == ord('v')):
					if (view_mode == ViewMode.Input):
						view_mode = ViewMode.Output
					else:
						view_mode = ViewMode.Input
					break
				elif (c == ord('m')):
					sticky_mode = not sticky_mode
					skip_input_processing = True
					break
				elif(c == ord('h')):
					last_view_mode = view_mode
					view_mode = ViewMode.Help
					break
				elif (c == curses.KEY_NPAGE):
					selected_device = InputDevice((int(selected_device) - 1) % 3)
					prev_states = [cur_state[selected_device]] * 3
					skip_input_processing = True
					break
				elif (c == curses.KEY_PPAGE):
					selected_device = InputDevice((int(selected_device) + 1) % 3)
					prev_states = [cur_state[selected_device]] * 3
					skip_input_processing = True
					break
					
				if (selected_device == InputDevice.SWITCHES):
					if (c in switches_keymap.keys()):
						n = switches_keymap[c]
						new_state = new_state ^ (1<<n)
				elif (selected_device == InputDevice.KEYS):
					if (c in keys_keymap.keys()):
						n = keys_keymap[c]
						new_state = new_state ^ (1<<n)
				elif (selected_device == InputDevice.N64_CONTROLLER):
					if (c in n64_controller_keymap.keys()):
						n = n64_controller_keymap[c]
						new_state = new_state ^ (1<<n)
					
					if (c in n64_controller_as_keymap.keys()):
						if(n64_controller_as_keymap[c][0] == "x"):
							new_x = (((new_state >> 16) & 0xff)+(n64_controller_as_keymap[c][1])) & 0xff
							new_state = (new_state & ~(0xff << 16)) | (new_x << 16) 
						else:
							new_y = (((new_state >> 24) & 0xff)+(n64_controller_as_keymap[c][1])) & 0xff
							new_state = (new_state & ~(0xff << 24)) | (new_y << 24) 

			if(not skip_input_processing):
				if (not sticky_mode):
					prev_states.pop()
					prev_states.insert(0, new_state)
					for x in prev_states:
						if(selected_device == InputDevice.KEYS):
							new_state &= x
						else:
							new_state |= x

				if(cur_state[selected_device] != new_state):
					remote.WriteAddress(selected_device.GetBaseAddress(), new_state)
					cur_state[selected_device] = new_state 


		elif(view_mode == ViewMode.Help):
			stdscr.clear()
			InteractiveModePrintHelpScreen(stdscr)
			stdscr.refresh()
			
			while(True):
				c = stdscr.getch()
				if (c == curses.KEY_END or c == 27):
					stop = True
					break
				elif (c == -1):
					pass
				elif ( c == ord('v')):
					view_mode = ViewMode.Input
					break
				elif(c == ord('h')):
					view_mode = last_view_mode
					break
	remote.Close()

def PrintValue(value, bit_selection=None):
	if (bit_selection == None):
		print(hex(value))
	else:
		bit_selection = bit_selection.split(":")
		if (len(bit_selection)==1):
			print(hex((value>>int(bit_selection[0],0) & 0x1)))
		elif (len(bit_selection)==2):
			a = int(bit_selection[0],0)
			b = int(bit_selection[1],0)
			if(a < b):
				print("invalid range specification")
				return
			print( hex( (~((-1)<<(a+1)) & value) >> b) )
		else:
			print("invalid bit selection")

def RMW(old_value, new_value,  bit_selection):
	bit_selection = bit_selection.split(':')
	new_value = int(new_value,0)
	if (len(bit_selection)==1):
		bit_selection = int(bit_selection[0],0)
		if( not (new_value == 0 or new_value == 1)):
			print("invlaid value specified " + str(new_value))
			return None
		return (old_value &  ~(1<<bit_selection)) | (new_value<<bit_selection) 

	elif (len(bit_selection)==2):
		a = int(bit_selection[0],0)
		b = int(bit_selection[1],0)
		if(a < b):
			print("invalid range specification")
			return
			
		mask = ((2**((a-b)+1)-1))
		
		if(~mask & new_value != 0):
			print("invalid value for this this range: " + str(hex(new_value)))
			return None
		
		return (old_value & ~(mask << b) | (new_value << b))
	else:
		print("invalid bit selection")
		return None

if __name__ == "__main__":
	options = docopt(usage_msg, version="0.1")
	#print(options)

	if (options["-k"] or options["-s"] or options["-n"]):
		remote = RemoteBridge()
		input_device = None 
		
		if(options["-k"]):
			input_device = InputDevice.KEYS
		elif(options["-s"]):
			input_device = InputDevice.SWITCHES
		elif(options["-n"]):
			input_device = InputDevice.N64_CONTROLLER
			
		address = input_device.GetBaseAddress()

		if(options["<VALUE>"] == None):
			value = remote.ReadAddress(address)
			if (options["-a"] == True):
				print(input_device.DrawDevice(value))
			else:
				PrintValue(value, options["-b"])
		else:
			if(options["-b"] != None):
				value = RMW(remote.ReadAddress(address), options["<VALUE>"], options["-b"])
			else:
				value = int(options["<VALUE>"],0)
			
			if(value != None):
				remote.WriteAddress(address, value)
		remote.Close()
	elif (options["-r"]):
		remote = RemoteBridge()
		ledr = remote.ReadAddress(ADDR_LEDR)
		if(options["-a"] == False):
			print(hex(ledr))
		else:
			print(DrawLEDs(ledr,18))
		remote.Close()
	elif (options["-g"]):
		remote = RemoteBridge()
		ledg = remote.ReadAddress(ADDR_LEDG)
		if(options["-a"] == False):
			print(hex(ledg))
		else:
			print(DrawLEDs(ledg,9))
		remote.Close()
	elif (options["-x"]):
		remote = RemoteBridge()
		
		hex_values_list = ReadHexValues(remote)
		hex_values_list.reverse()
		
		if(options["-a"] == False):
			for h in hex_values_list:
				print(hex(h)+" ", end="")
			print()
		else:
			print(Draw7SegmentChars(hex_values_list))
		remote.Close()
	elif (options["-i"]):
		curses.wrapper(InteractiveMode)
	elif (options["-p"] != None):
		subprocess.run(["quartus_pgm", "-m", "jtag" ,"-o" , "p;" + options["-p"] ])
