library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package remote_bridge_pkg is

	component remote_bridge is
		port (
			clk   : in std_logic;
			res_n    : in std_logic;
			
			switches : out std_logic_vector(17 downto 0);
			keys     : out std_logic_vector(3 downto 0);
			n64_data : inout std_logic;
			ledr : in std_logic_vector(17 downto 0);
			ledg : in std_logic_vector(8 downto 0);
			hex0 : in std_logic_vector(6 downto 0);
			hex1 : in std_logic_vector(6 downto 0);
			hex2 : in std_logic_vector(6 downto 0);
			hex3 : in std_logic_vector(6 downto 0);
			hex4 : in std_logic_vector(6 downto 0);
			hex5 : in std_logic_vector(6 downto 0);
			hex6 : in std_logic_vector(6 downto 0);
			hex7 : in std_logic_vector(6 downto 0)
		);
	end component;
end package;

