library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.sync_pkg.all;
use work.n64_controller_pkg.all;


entity n64_controller_emulator is
	port(
		clk : in std_logic;
		res_n : in std_logic;
		data : inout std_logic;
		button_state : n64_buttons_t
	);
end entity;


architecture arch of n64_controller_emulator is

	component n64_controller_emulator_top is
		port(
			clk : in std_logic;
			res_n : in std_logic;
			data : inout std_logic;
			btn_up     : std_logic;
			btn_down   : std_logic;
			btn_left   : std_logic;
			btn_right  : std_logic;
			btn_c_up   : std_logic;
			btn_c_down : std_logic;
			btn_c_left : std_logic;
			btn_c_right: std_logic;
			btn_start  : std_logic;
			btn_z      : std_logic;
			btn_l      : std_logic;
			btn_r      : std_logic;
			btn_a      : std_logic;
			btn_b      : std_logic;
			as_x       : std_logic_vector(7 downto 0);
			as_y       : std_logic_vector(7 downto 0)
		);
	end component;
begin
	-- just a wrapper
	emu : n64_controller_emulator_top
	port map(
		clk  => clk,
		res_n => res_n,
		data  => data,
		btn_up     => button_state.btn_up,
		btn_down   => button_state.btn_down,
		btn_left   => button_state.btn_left,
		btn_right  => button_state.btn_right,
		btn_c_up   => button_state.btn_c_up,
		btn_c_down => button_state.btn_c_down,
		btn_c_left => button_state.btn_c_left,
		btn_c_right=> button_state.btn_c_right,
		btn_start  => button_state.btn_start,
		btn_z      => button_state.btn_z,
		btn_l      => button_state.btn_l,
		btn_r      => button_state.btn_r,
		btn_a      => button_state.btn_a,
		btn_b      => button_state.btn_b,
		as_x       => button_state.as_x,
		as_y       => button_state.as_y
	);
end architecture;
