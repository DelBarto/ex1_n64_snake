library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.n64_controller_pkg.all;


package n64_controller_emulator_pkg is
	component n64_controller_emulator is
		port(
			clk : in std_logic;
			res_n : in std_logic;
			data : inout std_logic;
			button_state : in n64_buttons_t
		);
	end component;
end package;
