library ieee;
use ieee.numeric_std.all;
use ieee.std_logic_1164.all;

use work.textmode_controller_pkg.all;
use work.math_pkg.all;

package snake_pkg is
	-- DO NOT CHANGE THE SEQUENCE OF THIS TYPE
	-- Some function use the POS attribute and expect this sequence 
	type direction_t is (UP, DOWN, LEFT, RIGHT);
	
	--function to invert directions: not UP = DOWN, not LEFT = RIGHT
	function "NOT" (a : direction_t) return direction_t;
	
	
	type direction_array_t is array(integer range<>) of direction_t;
	type char_array_t is array(integer range<>) of std_logic_vector(7 downto 0);

	constant SNAKE_HEAD_UP    : std_logic_vector(7 downto 0) := x"90";
	constant SNAKE_HEAD_DOWN  : std_logic_vector(7 downto 0) := x"92";
	constant SNAKE_HEAD_LEFT  : std_logic_vector(7 downto 0) := x"91";
	constant SNAKE_HEAD_RIGHT : std_logic_vector(7 downto 0) := x"93";
	
	constant SNAKE_TAIL_UP    : std_logic_vector(7 downto 0) := x"8a";
	constant SNAKE_TAIL_DOWN  : std_logic_vector(7 downto 0) := x"88";
	constant SNAKE_TAIL_LEFT  : std_logic_vector(7 downto 0) := x"8b";
	constant SNAKE_TAIL_RIGHT : std_logic_vector(7 downto 0) := x"89";
	
	constant SNAKE_BODY_UP_LEFT    : std_logic_vector(7 downto 0) := x"A3";
	constant SNAKE_BODY_UP_RIGHT   : std_logic_vector(7 downto 0) := x"A2";
	constant SNAKE_BODY_DOWN_LEFT  : std_logic_vector(7 downto 0) := x"A0";
	constant SNAKE_BODY_DOWN_RIGHT : std_logic_vector(7 downto 0) := x"A1";
	
	constant SNAKE_BODY_HORIZONTAL  : std_logic_vector(7 downto 0) := x"82";
	constant SNAKE_BODY_VERTICAL : std_logic_vector(7 downto 0) := x"80";
	
	constant VEC2D_X_WIDTH : integer := log2c(COLUMN_COUNT-1);
	constant VEC2D_Y_WIDTH : integer := log2c(ROW_COUNT-1);
	
	type vec2d_t is 
	record
		x : std_logic_vector(VEC2D_X_WIDTH-1 downto 0);
		y : std_logic_vector(VEC2D_Y_WIDTH-1 downto 0);
	end record;
	
	constant NULL_VEC : vec2d_t := (x=>(others=>'0'), y=>(others=>'0'));
	
	type snake_position_t is 
	record
		head_position  : vec2d_t;
		head_direction : direction_t;
		tail_position  : vec2d_t;
		tail_direction : direction_t;
	end record;
	
	constant NULL_SNAKE_POSITION : snake_position_t := (
		head_position  => NULL_VEC,
		head_direction => UP,
		tail_position  => NULL_VEC,
		tail_direction => UP
	);
	
	function create_vec2d(x : integer; y : integer) return vec2d_t;
	function move_vec2d(vec: vec2d_t; dir : direction_t) return vec2d_t;
	
	function move_vec2d_and_wrap(vec: vec2d_t; dir : direction_t; x_max : integer; y_max :integer) return vec2d_t;
	
	function vec2d_to_txt_cntrl_data(vec: vec2d_t) return std_logic_vector;
	
	function get_head_character_from_direction(dir : direction_t) return std_logic_vector;
	function get_tail_character_from_direction(dir : direction_t) return std_logic_vector;
	
	function get_body_symbol_for_head_replacement(old_dir : direction_t; new_dir : direction_t) return std_logic_vector;
	function get_new_tail_direction(old_tail_dir : direction_t; cur_tail_symbol : std_logic_vector) return direction_t;
	
	component snake is
		port (
			clk : in std_logic;
			res_n : in std_logic;
			tm_instr_wr : out std_logic;
			tm_instr : out std_logic_vector(3 downto 0);
			tm_instr_data : out std_logic_vector(15 downto 0);
			tm_instr_result : in std_logic_vector(15 downto 0);
			tm_busy : in std_logic;
			color : in std_logic_vector(7 downto 0);
			init : in std_logic;
			init_head_position : in vec2d_t;
			init_head_direction : in direction_t;
			init_body_length : in std_logic_vector(3 downto 0);
			move_head : in std_logic;
			movement_direction : in direction_t;
			move_tail : in std_logic;
			done : out std_logic
		);
	end component;
end package;


package body snake_pkg is


	function create_vec2d(x : integer; y : integer) return vec2d_t is
		constant vec : vec2d_t := (x=>std_logic_vector(to_unsigned(x, log2c(COLUMN_COUNT-1))), y=>std_logic_vector(to_unsigned(y, log2c(ROW_COUNT-1))));
	begin
		return vec;
	end function;

	function move_vec2d(vec: vec2d_t; dir : direction_t) return vec2d_t is
		variable rvec : vec2d_t;
	begin
		rvec := vec;
		case dir is
			when UP =>
				rvec.y := std_logic_vector(unsigned(rvec.y) - 1);
			when DOWN =>
				rvec.y := std_logic_vector(unsigned(rvec.y) + 1);
			when LEFT =>
				rvec.x := std_logic_vector(unsigned(rvec.x) - 1);
			when RIGHT =>
				rvec.x := std_logic_vector(unsigned(rvec.x) + 1);
		end case;
		
		return rvec;
	end function;
	
	
	function move_vec2d_and_wrap(vec: vec2d_t; dir : direction_t; x_max : integer; y_max :integer) return vec2d_t is
		variable rvec : vec2d_t;
	begin
		rvec := vec;
		case dir is
			when UP =>
				if(unsigned(rvec.y) = 0) then
					rvec.y := std_logic_vector(to_unsigned(y_max,VEC2D_Y_WIDTH));
				else
					rvec.y := std_logic_vector(unsigned(rvec.y) - 1);
				end if;
			when DOWN =>
				if(unsigned(rvec.y) = y_max) then
					rvec.y := (others=>'0');
				else
					rvec.y := std_logic_vector(unsigned(rvec.y) + 1);
				end if;
			when LEFT =>
				if(unsigned(rvec.x) = 0) then
					rvec.x := std_logic_vector(to_unsigned(x_max,VEC2D_X_WIDTH));
				else
					rvec.x := std_logic_vector(unsigned(rvec.x) - 1);
				end if;
			when RIGHT =>
				if(unsigned(rvec.x) = x_max) then
					rvec.x := (others=>'0');
				else
					rvec.x := std_logic_vector(unsigned(rvec.x) + 1);
				end if;
		end case;
		
		return rvec;
	end function;
	
	function vec2d_to_txt_cntrl_data(vec: vec2d_t) return std_logic_vector is
		variable data : std_logic_vector(15 downto 0);
	begin
		data := (others=>'0');
		data(VEC2D_Y_WIDTH-1 downto 0) := vec.y;
		data(VEC2D_X_WIDTH-1+8 downto 8) := vec.x;
		return data;
	end function;
	
	function get_head_character_from_direction(dir : direction_t) return std_logic_vector is
		constant mapping : char_array_t(0 to 3) := (SNAKE_HEAD_UP, SNAKE_HEAD_DOWN, SNAKE_HEAD_LEFT, SNAKE_HEAD_RIGHT);
	begin
		return mapping(direction_t'pos(dir));
	end function;
	
	function get_tail_character_from_direction(dir : direction_t) return std_logic_vector is
		constant mapping : char_array_t(0 to 3) := (SNAKE_TAIL_UP, SNAKE_TAIL_DOWN, SNAKE_TAIL_LEFT, SNAKE_TAIL_RIGHT);
	begin
		return mapping(direction_t'pos(dir));
	end function;

	function "NOT" (a : direction_t) return direction_t is
		constant mapping : direction_array_t(0 to 3) := (DOWN, UP, RIGHT, LEFT);
	begin
		return mapping(direction_t'pos(a));
	end function;


	function get_body_symbol_for_head_replacement(old_dir : direction_t; new_dir : direction_t) return std_logic_vector is
	begin
		case old_dir is
			when UP =>
				case new_dir is
					when UP     => return SNAKE_BODY_VERTICAL;
					when LEFT   => return SNAKE_BODY_DOWN_LEFT;
					when RIGHT  => return SNAKE_BODY_DOWN_RIGHT;
					when others => return x"B1"; --invalid
				end case;
			when DOWN =>
				case new_dir is
					when DOWN   => return SNAKE_BODY_VERTICAL;
					when LEFT   => return SNAKE_BODY_UP_LEFT;
					when RIGHT  => return SNAKE_BODY_UP_RIGHT;
					when others => return x"B1"; --invalid
				end case;
			when LEFT =>
				case new_dir is
					when LEFT   => return SNAKE_BODY_HORIZONTAL;
					when UP     => return SNAKE_BODY_UP_RIGHT;
					when DOWN   => return SNAKE_BODY_DOWN_RIGHT;
					when others => return x"B1"; --invalid
				end case;
			when RIGHT =>
				case new_dir is
					when RIGHT  => return SNAKE_BODY_HORIZONTAL;
					when UP     => return SNAKE_BODY_UP_LEFT;
					when DOWN   => return SNAKE_BODY_DOWN_LEFT;
					when others => return x"B1"; --invalid
				end case;
		end case;
	end function;
	
	function get_new_tail_direction(old_tail_dir : direction_t; cur_tail_symbol : std_logic_vector) return direction_t is
	begin
		case old_tail_dir is
			when UP =>
				case cur_tail_symbol is
					when SNAKE_BODY_VERTICAL => return UP;
					when SNAKE_BODY_DOWN_LEFT => return LEFT;
					when SNAKE_BODY_DOWN_RIGHT => return RIGHT;
					when others => return UP; --invalid
				end case;
			when DOWN =>
				case cur_tail_symbol is
					when SNAKE_BODY_VERTICAL => return DOWN;
					when SNAKE_BODY_UP_LEFT => return LEFT;
					when SNAKE_BODY_UP_RIGHT => return RIGHT;
					when others => return UP; --invalid
				end case;
			when LEFT =>
				case cur_tail_symbol is
					when SNAKE_BODY_HORIZONTAL => return LEFT;
					when SNAKE_BODY_UP_RIGHT => return UP;
					when SNAKE_BODY_DOWN_RIGHT => return DOWN;
					when others => return UP; --invalid
				end case;
			when RIGHT =>
				case cur_tail_symbol is
					when SNAKE_BODY_HORIZONTAL => return RIGHT;
					when SNAKE_BODY_UP_LEFT => return UP;
					when SNAKE_BODY_DOWN_LEFT => return DOWN;
					when others => return UP; --invalid
				end case;
		end case;
	end function;
	
end package body;

