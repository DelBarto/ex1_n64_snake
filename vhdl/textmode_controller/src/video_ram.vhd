library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.math_pkg.all;


entity video_ram is
	generic (
		DATA_WIDTH : integer := 16;
		COLUMN_COUNT : integer := 100;
		ROW_COUNT : integer := 30
	);
	port (
		clk       : in std_logic;
		
		cntrl_data_in  : in std_logic_vector(DATA_WIDTH - 1 downto 0);
		cntrl_data_out : out std_logic_vector(DATA_WIDTH - 1 downto 0);
		cntrl_row_addr : in std_logic_vector(log2c(ROW_COUNT)-1 downto 0);
		cntrl_col_addr : in std_logic_vector(log2c(COLUMN_COUNT)-1 downto 0);
		cntrl_wr       : in std_logic;
		
		gc_data_out    : out std_logic_vector(DATA_WIDTH - 1 downto 0);
		gc_row_addr    : in std_logic_vector(log2c(ROW_COUNT)-1 downto 0);
		gc_col_addr    : in std_logic_vector(log2c(COLUMN_COUNT)-1 downto 0);
		gc_rd          : in std_logic;
		
		scroll_offset : in std_logic_vector(log2c(ROW_COUNT)-1 downto 0)
	);
end entity;

/*
architecture arch of video_ram is

	component tdp_ram is
		generic (
			DATA_WIDTH : natural := 8;
			ADDR_WIDTH : natural := 6
		);
		port (
			clk : in std_logic;
			addr_a : in std_logic_vector((ADDR_WIDTH-1) downto 0);
			addr_b : in std_logic_vector((ADDR_WIDTH-1) downto 0);
			data_a : in std_logic_vector((DATA_WIDTH-1) downto 0);
			data_b : in std_logic_vector((DATA_WIDTH-1) downto 0);
			we_a : in std_logic := '1';
			we_b : in std_logic := '1';
			q_a : out std_logic_vector((DATA_WIDTH -1) downto 0);
			q_b : out std_logic_vector((DATA_WIDTH -1) downto 0)
		);
	end component;
begin


	tdp_ram_inst : tdp_ram
	generic map (
		DATA_WIDTH => xxx,
		ADDR_WIDTH => xxx
	)
	port map (
		clk    => xxx,
		addr_a => xxx,
		addr_b => xxx,
		data_a => xxx,
		data_b => xxx,
		we_a   => xxx,
		we_b   => xxx,
		q_a    => xxx,
		q_b    => xxx
	);



end architecture;

architecture arch of video_ram is 
	type mem_type is array ( (2** (log2c(ROW_COUNT) + log2c(COLUMN_COUNT)))-1 downto 0) of std_logic_vector( DATA_WIDTH - 1 downto 0) ;
	signal ram : mem_type ;

	constant ROW_ADDR_WIDTH : integer := log2c(ROW_COUNT);
	constant COLUM_ADDR_WIDTH : integer := log2c(COLUMN_COUNT);
begin

	cntrl_port : process (clk)
		variable cntrl_row_address_scrolled : integer range 0 to 2**(log2c(ROW_COUNT)); 
		variable cntrl_address : std_logic_vector(log2c(ROW_COUNT) + log2c(COLUMN_COUNT)-1 downto 0);
		
		variable gc_row_address_scrolled : integer range 0 to 2**(log2c(ROW_COUNT));
		variable gc_addr : std_logic_vector(log2c(ROW_COUNT) + log2c(COLUMN_COUNT)-1 downto 0);
	begin
		if rising_edge(clk) then
		
			cntrl_row_address_scrolled := to_integer(unsigned(cntrl_row_addr) + unsigned(scroll_offset)); 
			if cntrl_row_address_scrolled > ROW_COUNT - 1 then
				cntrl_row_address_scrolled := cntrl_row_address_scrolled - ROW_COUNT;
			end if;
			cntrl_address := cntrl_col_addr & std_logic_vector(to_unsigned(cntrl_row_address_scrolled, ROW_ADDR_WIDTH));

			if cntrl_wr = '1' then
				ram(to_integer(unsigned(cntrl_address))) <= cntrl_data_in;
			end if;
		
			-- read from ram
			--if gc_rd = '1' then
				gc_row_address_scrolled := to_integer(unsigned(gc_row_addr) + unsigned(scroll_offset)); 
				if gc_row_address_scrolled > ROW_COUNT - 1 then
					gc_row_address_scrolled := gc_row_address_scrolled - ROW_COUNT;
				end if;
				gc_addr := gc_col_addr & std_logic_vector(to_unsigned(gc_row_address_scrolled, ROW_ADDR_WIDTH));
				gc_data_out <= ram(to_integer(unsigned(gc_addr)));
			--end if;
		end if;
	end process;

*/


architecture arch of video_ram is 
	type mem_type is array ( (2** (log2c(ROW_COUNT) + log2c(COLUMN_COUNT)))-1 downto 0) of std_logic_vector( DATA_WIDTH - 1 downto 0) ;
	shared variable ram : mem_type ;

	constant ROW_ADDR_WIDTH : integer := log2c(ROW_COUNT);
	constant COLUM_ADDR_WIDTH : integer := log2c(COLUMN_COUNT);
begin

	cntrl_port : process (clk)
		variable cntrl_row_address_scrolled : integer range 0 to 2**(log2c(ROW_COUNT)); 
		variable cntrl_address : std_logic_vector(log2c(ROW_COUNT) + log2c(COLUMN_COUNT)-1 downto 0);
	begin
		if rising_edge(clk) then
			cntrl_row_address_scrolled := to_integer(unsigned(cntrl_row_addr) + unsigned(scroll_offset)); 
			if cntrl_row_address_scrolled > ROW_COUNT - 1 then
				cntrl_row_address_scrolled := cntrl_row_address_scrolled - ROW_COUNT;
			end if;
			cntrl_address := cntrl_col_addr & std_logic_vector(to_unsigned(cntrl_row_address_scrolled, ROW_ADDR_WIDTH));

			if cntrl_wr = '1' then
				ram(to_integer(unsigned(cntrl_address))) := cntrl_data_in;
				cntrl_data_out <= cntrl_data_in;
			else 
				cntrl_data_out <= ram(to_integer(unsigned(cntrl_address)));
			end if;
		end if;
	end process;

	gc_port : process (clk)
		variable gc_row_address_scrolled : integer range 0 to 2**(log2c(ROW_COUNT));
		variable gc_addr : std_logic_vector(log2c(ROW_COUNT) + log2c(COLUMN_COUNT)-1 downto 0);
	begin
		if rising_edge(clk) then
			gc_row_address_scrolled := to_integer(unsigned(gc_row_addr) + unsigned(scroll_offset)); 
			if gc_row_address_scrolled > ROW_COUNT - 1 then
				gc_row_address_scrolled := gc_row_address_scrolled - ROW_COUNT;
			end if;
			gc_addr := gc_col_addr & std_logic_vector(to_unsigned(gc_row_address_scrolled, ROW_ADDR_WIDTH));
			gc_data_out <= ram(to_integer(unsigned(gc_addr)));
		end if;
	end process;
	

end architecture;

