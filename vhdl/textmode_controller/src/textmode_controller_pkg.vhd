library ieee;
use ieee.std_logic_1164.all;
use work.display_controller_pkg.all;
use work.font_pkg.all;

package textmode_controller_pkg is

	constant ROW_COUNT : integer := DISPLAY_HEIGHT/CHAR_HEIGHT;
	constant COLUMN_COUNT : integer := DISPLAY_WIDTH/CHAR_WIDTH;

	-- cursor options
	constant CURSOR_STATE_OFF : std_logic_vector(1 downto 0) := "00";
	constant CURSOR_STATE_ON : std_logic_vector(1 downto 0) := "01";
	constant CURSOR_STATE_BLINK : std_logic_vector(1 downto 0) := "11";

	subtype instr_t is std_logic_vector(3 downto 0);

	--Instructions for this textmode video controller
	constant INSTR_NOP : instr_t := (others=>'0');
	constant INSTR_SET_CHAR : instr_t := x"1";
	constant INSTR_CLEAR_SCREEN : instr_t := x"2";
	constant INSTR_SET_CURSOR_POSITION : instr_t := x"3";
	constant INSTR_CFG : instr_t := x"4";
	constant INSTR_DELETE : instr_t := x"5";
	constant INSTR_MOVE_CURSOR_NEXT : instr_t := x"6";
	constant INSTR_NEW_LINE : instr_t := x"7";
	constant INSTR_GET_CHAR : instr_t := x"8";
	
	component textmode_controller is
		generic (
			CLK_FREQ : integer := 25000000
		);
		port (
			clk : in std_logic;
			res_n : in std_logic;
			instr_wr : in std_logic;
			instr : in std_logic_vector(3 downto 0);
			instr_data : in std_logic_vector(15 downto 0);
			instr_result : out std_logic_vector(15 downto 0);
			busy : out std_logic;
			vblank : out std_logic;
			hd : out std_logic;
			vd : out std_logic;
			den : out std_logic;
			r : out std_logic_vector(7 downto 0);
			g : out std_logic_vector(7 downto 0);
			b : out std_logic_vector(7 downto 0);
			grest : out std_logic; -- display reset
			nclk  : out std_logic  -- display clk
		);
	end component;

end package;







