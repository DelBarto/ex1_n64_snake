library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use work.math_pkg.all;
use work.display_controller_pkg.all;
use work.font_pkg.all;

entity display_controller is
	generic (
		COLUMN_COUNT : integer := 100;
		ROW_COUNT : integer := 30
	);
	port (
		clk   : in  std_logic;   -- global system clk 
		res_n : in  std_logic;   -- system reset

		-- connection video ram
		vram_addr_row   : out std_logic_vector(log2c(ROW_COUNT)-1 downto 0);
		vram_addr_colum : out std_logic_vector(log2c(COLUMN_COUNT)-1 downto 0);
		vram_data       : in std_logic_vector(15 downto 0);
		vram_rd         : out std_logic;

		-- connection to font rom
		char              : out std_logic_vector(log2c(CHAR_COUNT)-1 downto 0);
		char_height_pixel : out std_logic_vector(log2c(CHAR_HEIGHT)-1 downto 0);
		decoded_char      : in std_logic_vector(0 to CHAR_WIDTH-1);

		vblank : out std_logic;

		-- connection to display
		hd   : out std_logic;         -- horizontal sync signal
		vd   : out std_logic;            -- vertical sync signal
		den  : out std_logic;            -- data enable 
		r    : out std_logic_vector(7 downto 0);  -- pixel color value (red)
		g    : out std_logic_vector(7 downto 0);  -- pixel color value (green)
		b    : out std_logic_vector(7 downto 0);  -- pixel color value (blue)

		grest : out std_logic -- display reset
	);
end entity;


architecture beh of display_controller is

	constant CLK_PER_HORIZONTAL_LINE : integer := 1056;
	constant H_LINE_PER_FRAME : integer := 525;

	constant H_SYNC_BACK_PORCH : integer := 216;   -- in CLK-periods
	constant H_SYNC_FRONT_PORCH : integer := 40;   -- in CLK-periods

	constant VERTICAL_BACK_PORCH : integer := 35;  -- in Horizontal lines
	constant VERTICAL_FRONT_PORCH : integer := 10; -- in Horizontal lines

	--constant CHARS_PER_HORIZONTAL_LINE : integer := CLK_PER_HORIZONTAL_LINE / 8;

	signal vertical_display_area : std_logic;
	
	signal pixel_cnt_x : std_logic_vector(log2c(CLK_PER_HORIZONTAL_LINE)-1 downto 0);
	
	
	signal char_pixel_cnt : std_logic_vector(log2c(CHAR_WIDTH)-1 downto 0);

	
	signal hline_cnt : integer range 0 to H_LINE_PER_FRAME;
	
	signal char_pixel_buffer : std_logic_vector(0 to CHAR_WIDTH-1);
	
	signal attribute_buffer : std_logic_vector(7 downto 0);
	
begin
	grest <= res_n;
	
	snyc : process(clk, res_n)
	begin
		if res_n = '0' then -- reset
			--reset counter signals
			pixel_cnt_x <= (others=>'0');
			hline_cnt <= 0;
		elsif rising_edge(clk) then
			pixel_cnt_x <= pixel_cnt_x + 1; -- inc clk counter 
			
			if pixel_cnt_x = CLK_PER_HORIZONTAL_LINE - 1  then -- line complete
				pixel_cnt_x <= (others=>'0');
				hline_cnt <= hline_cnt + 1; --inc line counter

				-- check for line counter overflow --> frame complete
				if hline_cnt = H_LINE_PER_FRAME - 1 then  
					hline_cnt <= 0;
				end if;
			end if;
		end if;
	end process;

	output : process(all)
		variable color_index : integer range 0 to 15;
	begin

		-- hd and vd are low active
		hd <= '1'; 
		vd <= '1';

		-- generate the horizontal sync pulse, however make sure that the signal stays high during reset
		if (pixel_cnt_x = 0 and res_n = '1') then 
			hd <= '0';
		end if;

		-- generate the vertical sync pulse
		if (hline_cnt = 0 and res_n = '1') then 
			vd <= '0';
		end if;

		r <= (others => '0');
		g <= (others => '0');
		b <= (others => '0');
		den <= '0';

		if vertical_display_area = '1' then 
			if pixel_cnt_x >= (H_SYNC_BACK_PORCH) and 
				pixel_cnt_x < (CLK_PER_HORIZONTAL_LINE - H_SYNC_FRONT_PORCH) then

				-- char_pixel_buffer
				-- attribute_buffer
		
				if char_pixel_buffer(to_integer(unsigned(char_pixel_cnt))) = '1' then --foreground
					color_index := to_integer(unsigned(attribute_buffer(7 downto 4)));
				else --background
					color_index := to_integer(unsigned(attribute_buffer(3 downto 0)));
				end if;
			
				r <= COLOR_TABLE(color_index)(23 downto 16);
				g <= COLOR_TABLE(color_index)(15 downto 8);
				b <= COLOR_TABLE(color_index)(7 downto 0);
				den <= '1'; 
			end if;
		end if;
	end process;


	fetch : process(clk, res_n )
	begin
		if (res_n = '0') then
			vram_addr_row <= (others=>'0');
			vram_addr_colum <= (others=>'0');
			vram_rd <= '0';

			char <= (others=>'0');
			char_height_pixel <= (others=>'0');
			
			char_pixel_cnt <= (others=>'0');
			
			char_pixel_buffer <= (others=>'0');
			attribute_buffer <= (others=>'0');
		elsif (rising_edge(clk)) then
			
			if (vd='0') then
				vram_addr_row <= (others=>'0');
				vram_addr_colum <= (others=>'0');

				char <= (others=>'0');
				char_height_pixel <= (others=>'0');
				
				char_pixel_cnt <= (others=>'0');
				vram_rd <= '0';
			end if;
			
			vram_rd <= '0';
			
			char_pixel_cnt <= (others=>'0');
			
			if ( pixel_cnt_x >= (H_SYNC_BACK_PORCH-CHAR_WIDTH) and vertical_display_area = '1') then
				--char_pixel_cnt must continue to count up after the last fetch of the line, because output process depends on it 
				char_pixel_cnt <= std_logic_vector(unsigned(char_pixel_cnt) + 1); 
			
				if (
				  pixel_cnt_x < (CLK_PER_HORIZONTAL_LINE-H_SYNC_FRONT_PORCH-CHAR_WIDTH) 
				) then
					if(char_pixel_cnt = 0) then
						vram_rd <= '1';
					else
						vram_rd <= '0';
						char <= vram_data(7 downto 0);
					end if;
					
					if(unsigned(char_pixel_cnt) = CHAR_WIDTH-1) then
						char_pixel_cnt <= (others=>'0');
						char_pixel_buffer <= decoded_char;
						attribute_buffer <= vram_data(15 downto 8);
						
						if (vram_addr_colum = COLUMN_COUNT-1) then
							vram_addr_colum <= (others=>'0');
							if (char_height_pixel = CHAR_HEIGHT-1) then
								char_height_pixel <= (others=>'0');
								vram_addr_row <= std_logic_vector(unsigned(vram_addr_row) + 1);
							else
								char_height_pixel <= std_logic_vector(unsigned(char_height_pixel) + 1);
							end if;
						else
							vram_addr_colum <= std_logic_vector(unsigned(vram_addr_colum) + 1);
						end if;
					end if;
				end if;
			end if;
		end if;
	end process;
	

	vertical_display_area_checker : process(all) 
	begin
		vertical_display_area <= '0';
		if ( hline_cnt >= VERTICAL_BACK_PORCH) and --vertical back porch
		   ( hline_cnt < (H_LINE_PER_FRAME - VERTICAL_FRONT_PORCH) ) then --vertical front porch
			vertical_display_area <= '1';
		end if;
	end process;
	
	vblank <= not vertical_display_area;
end architecture;


