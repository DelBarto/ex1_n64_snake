library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.math_pkg.all;
use work.font_pkg.all;

entity font_rom is
	port (
		clk               : in std_logic;
		char              : in std_logic_vector(log2c(CHAR_COUNT) - 1 downto 0);
		char_height_pixel : in std_logic_vector(log2c(CHAR_HEIGHT) - 1 downto 0);
		decoded_char      : out std_logic_vector(0 to CHAR_WIDTH - 1)
	);
end entity;


architecture arch of font_rom is
begin
	process(clk)
		variable address_pre : std_logic_vector(log2c(CHAR_COUNT) + log2c(CHAR_HEIGHT+1) - 1 downto 0);
		variable address : std_logic_vector(log2c(CHAR_COUNT*CHAR_HEIGHT-1) - 1 downto 0);
	begin
		if rising_edge(clk) then
			address_pre := std_logic_vector(unsigned(char)*to_unsigned(CHAR_HEIGHT, log2c(CHAR_HEIGHT+1)) + unsigned(char_height_pixel));
			address := address_pre(address'range);
			--address := std_logic_vector( unsigned(char)*CHAR_HEIGHT + unsigned(char_height_pixel))(address'range);
			decoded_char <= FONT_TABLE(to_integer(unsigned(address)));
		end if;
	end process;
end architecture;
