library ieee;
use ieee.std_logic_1164.all;

use work.math_pkg.all;
use work.textmode_controller_pkg.all;
use work.font_pkg.all;
use work.display_controller_pkg.all;


entity textmode_controller is
	generic (
		CLK_FREQ : integer := 25000000
	);
	port (
		clk   : in  std_logic;
		res_n : in  std_logic;
		
		instr_wr    : in std_logic;
		instr       : in std_logic_vector(3 downto 0);
		instr_data  : in std_logic_vector(15 downto 0);
		instr_result : out std_logic_vector(15 downto 0);
		busy        : out std_logic;
		
		vblank : out std_logic;
		
		hd    : out std_logic;  -- horizontal sync signal
		vd    : out std_logic;  -- vertical sync signal
		den   : out std_logic;  -- data enable 
		r     : out std_logic_vector(7 downto 0); -- pixel color value (red)
		g     : out std_logic_vector(7 downto 0); -- pixel color value (green)
		b     : out std_logic_vector(7 downto 0); -- pixel color value (blue)
		grest : out std_logic; -- display reset
		nclk  : out std_logic  -- display clk
	);
end entity;


architecture arch of textmode_controller is 
	component textmode_controller_fsm is
		generic (
			ROW_COUNT : integer;
			COLUMN_COUNT : integer
		);
		port (
			clk : in  std_logic;
			res_n : in  std_logic;
		
			wr : in std_logic;
			busy : out std_logic;

			instr : in std_logic_vector(3 downto 0);
			instr_data : in std_logic_vector(15 downto 0);
			instr_result : out std_logic_vector(15 downto 0);
		
			scroll_offset : out std_logic_vector(log2c(ROW_COUNT)-1 downto 0);
			cursor_position_row : out std_logic_vector(log2c(ROW_COUNT)-1 downto 0);
			cursor_position_colum : out std_logic_vector(log2c(COLUMN_COUNT)-1 downto 0);
			cursor_color : out std_logic_vector(3 downto 0);
			cursor_state : out std_logic_vector(1 downto 0);
		
			video_ram_row_addr : out std_logic_vector(log2c(ROW_COUNT)-1 downto 0);
			video_ram_col_addr : out std_logic_vector(log2c(COLUMN_COUNT)-1 downto 0);
			video_ram_data_in : out std_logic_vector(15 downto 0);
			video_ram_data_out : in std_logic_vector(15 downto 0);
			video_ram_wr : out std_logic
		);
	end component;
	
	component cursor_controller is
		generic (
			CLK_FREQ : integer;
			BLINK_PERIOD : time range 1 ms to 2000 ms;
			ROW_COUNT : integer;
			COLUMN_COUNT : integer;
			COLOR_WIDTH : integer
		);
		port (
			clk : in std_logic;
			res_n : in std_logic;
			cursor_state : in std_logic_vector(1 downto 0);
			cursor_color : in std_logic_vector(COLOR_WIDTH-1 downto 0);
			position_row : in std_logic_vector(log2c(ROW_COUNT)-1 downto 0);
			position_colum : in std_logic_vector(log2c(COLUMN_COUNT)-1 downto 0);
			vram_addr_row : in std_logic_vector(log2c(ROW_COUNT)-1 downto 0);
			vram_addr_colum : in std_logic_vector(log2c(COLUMN_COUNT)-1 downto 0);
			vram_rd : in std_logic;
			vram_data_color_in : in std_logic_vector(COLOR_WIDTH-1 downto 0);
			vram_data_color_out : out std_logic_vector(COLOR_WIDTH-1 downto 0)
		);
	end component;
	
	component video_ram is
		generic (
			DATA_WIDTH : integer := 16;
			COLUMN_COUNT : integer := 100;
			ROW_COUNT : integer := 30
		);
		port (
			clk : in std_logic;
			cntrl_data_in : in std_logic_vector(DATA_WIDTH - 1 downto 0);
			cntrl_data_out : out std_logic_vector(DATA_WIDTH - 1 downto 0);
			cntrl_row_addr : in std_logic_vector(log2c(ROW_COUNT)-1 downto 0);
			cntrl_col_addr : in std_logic_vector(log2c(COLUMN_COUNT)-1 downto 0);
			cntrl_wr : in std_logic;
			gc_data_out : out std_logic_vector(DATA_WIDTH - 1 downto 0);
			gc_row_addr : in std_logic_vector(log2c(ROW_COUNT)-1 downto 0);
			gc_col_addr : in std_logic_vector(log2c(COLUMN_COUNT)-1 downto 0);
			gc_rd : in std_logic;
			scroll_offset : in std_logic_vector(log2c(ROW_COUNT)-1 downto 0)
		);
	end component;
	
	--signals between controller and video ram
	signal cntrl_vram_row_addr : std_logic_vector(log2c(ROW_COUNT)-1 downto 0);
	signal cntrl_vram_col_addr : std_logic_vector(log2c(COLUMN_COUNT)-1 downto 0);
	signal cntrl_vram_wr : std_logic;
	signal cntrl_vram_data_in : std_logic_vector(15 downto 0); 
	signal cntrl_vram_data_out : std_logic_vector(15 downto 0); 
	signal scroll_offset_ram : std_logic_vector(log2c(ROW_COUNT)-1 downto 0);

	--signals between video ram and display controller
	signal rd_data_char_ram : std_logic_vector(7 downto 0);
	signal rd_data_bg_cc_in_ram : std_logic_vector(3 downto 0);
	signal rd_data_bg_cc_out_ram : std_logic_vector(3 downto 0);
	
	signal rd_data_fg_ram : std_logic_vector(3 downto 0);
	
	signal gc_vram_row_addr : std_logic_vector(log2c(ROW_COUNT)-1 downto 0);
	signal gc_vram_col_addr : std_logic_vector(log2c(COLUMN_COUNT)-1 downto 0);
	signal gc_vram_rd : std_logic;
	
	--signals between font rom and display controller
	signal rom_decoded_char : std_logic_vector(CHAR_WIDTH-1 downto 0);
	signal font_rom_char              : std_logic_vector(log2c(CHAR_COUNT) - 1 downto 0);
	signal font_rom_char_height_pixel : std_logic_vector(log2c(CHAR_HEIGHT) - 1 downto 0);
	
	-- signals between controller and cursor controller
	signal cursor_position_row : std_logic_vector(log2c(ROW_COUNT)-1 downto 0);
	signal cursor_position_colum : std_logic_vector(log2c(COLUMN_COUNT)-1 downto 0);
	signal cursor_color : std_logic_vector(3 downto 0);
	signal cursor_state : std_logic_vector(1 downto 0);
	
begin
	nclk <= clk;
	
	controller : textmode_controller_fsm
	generic map (
		ROW_COUNT => ROW_COUNT,
		COLUMN_COUNT => COLUMN_COUNT
	)
	port map (
		clk => clk,
		res_n => res_n,
		
		wr => instr_wr,
		busy => busy,
		instr => instr,
		instr_data => instr_data,
		instr_result => instr_result,
		
		video_ram_row_addr => cntrl_vram_row_addr,
		video_ram_col_addr => cntrl_vram_col_addr,
		video_ram_data_in  => cntrl_vram_data_in,
		video_ram_data_out => cntrl_vram_data_out,
		video_ram_wr       => cntrl_vram_wr,
		
		scroll_offset => scroll_offset_ram,
		cursor_position_row => cursor_position_row,
		cursor_position_colum => cursor_position_colum,
		cursor_color => cursor_color,
		cursor_state => cursor_state
	);

	video_ram_inst : video_ram 
	generic map (
		DATA_WIDTH => 16,
		ROW_COUNT => ROW_COUNT,
		COLUMN_COUNT => COLUMN_COUNT
	)
	port map(
		clk => clk,
		
		
		cntrl_data_in   => cntrl_vram_data_in,
		cntrl_data_out  => cntrl_vram_data_out,
		cntrl_row_addr  => cntrl_vram_row_addr,
		cntrl_col_addr  => cntrl_vram_col_addr,
		cntrl_wr        => cntrl_vram_wr,
		
		gc_row_addr    => gc_vram_row_addr,
		gc_col_addr    => gc_vram_col_addr,
		gc_rd         => gc_vram_rd,
		gc_data_out(7 downto 0) => rd_data_char_ram,
		gc_data_out(11 downto 8) => rd_data_bg_cc_in_ram,
		gc_data_out(15 downto 12) => rd_data_fg_ram,
		
		
		scroll_offset => scroll_offset_ram
	);
	
	
	font_rom_inst : font_rom
	port map (
		clk => clk,
		char => font_rom_char,
		char_height_pixel => font_rom_char_height_pixel,
		decoded_char => rom_decoded_char 
	);

	
	display_controller_inst : display_controller
	generic map (
		COLUMN_COUNT => COLUMN_COUNT,
		ROW_COUNT => ROW_COUNT
	)
	port map (
		clk => clk,
		res_n => res_n,

		-- connection video ram
		vram_addr_row =>  gc_vram_row_addr,
		vram_addr_colum =>  gc_vram_col_addr,
		vram_data(7 downto 0) => rd_data_char_ram, --character
		vram_data(11 downto 8) => rd_data_bg_cc_out_ram, -- background color
		vram_data(15 downto 12) => rd_data_fg_ram, -- foreground color
		vram_rd => gc_vram_rd,

		-- connection to font rom
		char => font_rom_char,
		char_height_pixel => font_rom_char_height_pixel,
		decoded_char => rom_decoded_char,

		vblank => vblank,

		-- connection to display
		hd => hd,
		vd => vd,
		den => den,
		r => r,
		g => g,
		b => b,
		grest => grest
	);
		
	
	cursor_controller_inst : cursor_controller
	generic map (
		CLK_FREQ => CLK_FREQ,
		BLINK_PERIOD => 1000 ms,
		ROW_COUNT => ROW_COUNT,
		COLUMN_COUNT => COLUMN_COUNT,
		COLOR_WIDTH => 4
	)
	port map (
		clk => clk, 
		res_n => res_n,

		cursor_state => cursor_state,
		cursor_color => cursor_color,
	
		position_row => cursor_position_row,
		position_colum => cursor_position_colum,
	
		vram_addr_row =>  gc_vram_row_addr,
		vram_addr_colum =>  gc_vram_col_addr,
		vram_rd => gc_vram_rd,
		
		vram_data_color_in => rd_data_bg_cc_in_ram,
		vram_data_color_out => rd_data_bg_cc_out_ram
	);

end architecture;

