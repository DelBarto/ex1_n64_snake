onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider clocks
add wave -noupdate /textmode_controller_tb/nclk
add wave -noupdate /textmode_controller_tb/vd
add wave -noupdate /textmode_controller_tb/hd
add wave -noupdate /textmode_controller_tb/den
add wave -noupdate -divider colors
add wave -noupdate /textmode_controller_tb/r
add wave -noupdate /textmode_controller_tb/g
add wave -noupdate /textmode_controller_tb/b

