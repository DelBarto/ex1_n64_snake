
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

use std.textio.all;
use ieee.std_logic_textio.all;

use work.ppm_writer_pkg.all;

entity ppm_writer is
	generic (
		OUTPUT_DIRECTORY : string := "./"
	);
	port (
		nclk  : in std_logic;
		grest : in std_logic;
		hd    : in std_logic; -- horizontal sync signal
		vd    : in std_logic; -- vertical sync signal
		den   : in std_logic; -- data enable
		r     : in std_logic_vector(7 downto 0);  -- pixel color value (red)
		g     : in std_logic_vector(7 downto 0);  -- pixel color value (green)
		b     : in std_logic_vector(7 downto 0)   -- pixel color value (blue)
	);
end entity;

architecture arch of ppm_writer is
  -- signal nclk  : std_logic;
	-- signal grest : std_logic;
	-- signal hd    : std_logic;
	-- signal vd    : std_logic;
	-- signal den   : std_logic;
	-- signal r     : std_logic_vector(7 downto 0);
	-- signal g     : std_logic_vector(7 downto 0);
	-- signal b     : std_logic_vector(7 downto 0);
begin
	-- TASK 4

	-- process
	-- 	variable widt : integer := 0;
	-- 	variable heig : integer := 0;
	-- begin
	-- 	-- if (widt = 0 and heig = 0) then
	-- 	wait until rising_edge(vd);
	--
	-- 	while vd = '1' loop
	-- 		-- if (widt = 0) then
	-- 		-- 	wait until (den = '1');
	-- 		-- 	wait until falling_edge(nclk);
	-- 		-- 	while (den = '1') loop
	-- 		-- 		wait until falling_edge(nclk);
	-- 		-- 		widt := widt + 1;
	-- 		-- 		-- report "----------width: " & to_string(widt);
	-- 		-- 		report "width: " & to_string(widt);
	-- 		-- 	end loop;
	-- 		-- 	heig := heig + 1;
	-- 		-- else
	-- 		wait until (den = '1');
	-- 		heig := heig + 1;
	-- 		-- end if;
	-- 		if vd = '0' then
	-- 			report "height: " & to_string(heig);
	-- 			report "WWWWWWWWWWWWIIIIINNNNNN";
	-- 		end if;
	-- 	end loop;
	--
	-- 	else
	-- 		-- BILD SPEICHERN
	-- 	end if;
	--
	-- end process;



	resol : process
		variable widt : integer := 0;
		variable heig : integer := 0;
		FILE disp_file : text;
		variable fcounter : integer := 1;
		variable lin : line;
	begin
		if ((widt = 0) and (heig = 0)) then
			wait until rising_edge(vd);
			while true loop
				exit when vd = '0';
				wait until rising_edge(hd);
				wait until rising_edge(den) or falling_edge(hd);
				if den = '1' then
					heig := heig + 1;
					if widt = 0 then
						wait until falling_edge(nclk);
						while den = '1' loop
							widt := widt + 1;
							wait until falling_edge(nclk);
						end loop;
					end if;
				end if;
			end loop;
			report "resolution: " & to_string(widt) & "x" & to_string(heig);

		else
			file_open(disp_file,
								OUTPUT_DIRECTORY & to_string(fcounter) & ".ppm",
								WRITE_MODE);

			write(lin, STRING'("P3"));
			writeline(disp_file, lin);
			write(lin, to_string(widt) & " " & to_string(heig) & " 255");
			writeline(disp_file, lin);

			wait until rising_edge(vd);
			while true loop
				exit when vd = '0';
				wait until rising_edge(hd);
				while true loop
					exit when hd = '0';
					wait until rising_edge(den) or falling_edge(hd);
					if (den = '1') then
						while true loop
							exit when den = '0';
							wait until rising_edge(nclk);
							if ((nclk = '1') and (den = '1')) then
								write(lin,
										to_string(unsigned(r)) & " " &
										to_string(unsigned(g)) & " " &
										to_string(unsigned(b)));
								writeline(disp_file, lin);
							end if;
						end loop;
					end if;
				end loop;
			end loop;
			file_close(disp_file);
			fcounter := fcounter + 1;
		end if;
	end process;







	-- process(den)
	-- begin
	-- 	report "ppm_writer den: " & to_string(den);
	-- end process;

-- 	p_dump  : process()
--         file        ppm_file    : text open write_mode is "output_file.txt";
--         variable    row         : line;
--     begin
--
--         if(i_rstb='0') then
--
--
--         elsif(rising_edge(i_clk)) then
--
--             if(o_valid = '1') then
--
--             write(row,o_add, right, 15);
--
--             write(row,conv_integer(o_add), right, 15);
--
--             hwrite(row,o_add, right, 15);
--
--             hwrite(row,"00000000"&o_add, right, 15);
--
--             writeline(test_vector,row);
--
--             end if;
--         end if;
--     end process p_dump;

end architecture;
