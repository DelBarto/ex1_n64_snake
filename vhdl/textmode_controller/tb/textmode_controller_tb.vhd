-- TASK 5: put your code here

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.textmode_controller_pkg.all;
use work.ppm_writer_pkg.all;

entity textmode_controller_tb is
end entity;

architecture bench of textmode_controller_tb is
	component textmode_controller is
		generic (
			CLK_FREQ : integer := 25000000
		);
		port (
			clk : in std_logic;
			res_n : in std_logic;

			instr_wr : in std_logic;
			instr : in std_logic_vector(3 downto 0);
			instr_data : in std_logic_vector(15 downto 0);
			instr_result : out std_logic_vector(15 downto 0);
			busy : out std_logic;

			vblank : out std_logic;

			hd : out std_logic;
			vd : out std_logic;
			den : out std_logic;
			r : out std_logic_vector(7 downto 0);
			g : out std_logic_vector(7 downto 0);
			b : out std_logic_vector(7 downto 0);
			grest : out std_logic; -- display reset
			nclk : out std_logic -- display clk
		);
	end component;

	signal clk : std_logic;
	signal res_n : std_logic;
	signal instr_wr : std_logic;
	signal instr : std_logic_vector(3 downto 0);
	signal instr_data : std_logic_vector(15 downto 0);
	signal instr_result : std_logic_vector(15 downto 0);
	signal busy : std_logic;
	signal vblank : std_logic;
	signal hd : std_logic;
	signal vd : std_logic;
	signal den : std_logic;
	signal r : std_logic_vector(7 downto 0);
	signal g : std_logic_vector(7 downto 0);
	signal b : std_logic_vector(7 downto 0);
	signal grest : std_logic; -- display reset
	signal nclk : std_logic; -- display clk

	constant CLK_PERIOD : time := 40 ns; -- 1 000 000 000 / 25 000 000
	signal stop_clock : boolean := false;
begin
	utt : textmode_controller
	port map(
		clk => clk,
		res_n => res_n,
		instr_wr => instr_wr,
		instr => instr,
		instr_data => instr_data,
		instr_result => instr_result,
		busy => busy,
		vblank => vblank,
		hd => hd,
		vd => vd,
		den => den,
		r => r,
		g => g,
		b => b,
		grest => grest,
		nclk => nclk
	);

	ppm_inst : ppm_writer
		port map (
			nclk => nclk,
			grest => grest,
			hd => hd,
			vd => vd,
			den => den,
			r => r,
			g => g,
			b => b
		);

	stimulus : process
		variable x : integer := 0; -- row
		variable y : integer := 0; -- columns

		constant widt : integer := 16;
		constant high : integer := 16;

		constant black : std_logic_vector(3 downto 0) := "0000";
		constant white : std_logic_vector(3 downto 0) := "1111";
		variable ascii : std_logic_vector(7 downto 0) := "00000000";
	begin
		report "-----START------";
		-- initialisation


		res_n <= '0';
		instr_wr <= '0';
		instr <= (others => '0');
		instr_data <= (others => '0');
		wait for CLK_PERIOD;
		res_n <= '1';
		wait for CLK_PERIOD;

		-- Config cursor - Disable cursor
		instr <= INSTR_CFG;
		instr_data <= "0000000000000000";

		instr_wr <= '1';
		wait for CLK_PERIOD;
		instr_wr <= '0';
		wait until (instr_wr = '0');
		wait until falling_edge(busy);

		-- Clear Screen
		instr <= INSTR_CLEAR_SCREEN;
		instr_data <= black & black & ascii;

		instr_wr <= '1';
		wait for CLK_PERIOD;
		instr_wr <= '0';
		wait until (instr_wr = '0');
		wait until falling_edge(busy);
		wait until vblank = '1';

		-- write ascii to screen
		for x in 0 to high - 1 loop
			for y in 0 to widt - 1 loop
				-- set cursor to new position
				instr <= INSTR_SET_CURSOR_POSITION;
				instr_data <= std_logic_vector(to_unsigned(y, 8)) &
								std_logic_vector(to_unsigned(x, 8));
				instr_wr <= '1';
				wait for CLK_PERIOD;
				instr_wr <= '0';
				wait until falling_edge(busy);

				-- write char
				instr <= INSTR_SET_CHAR;
				instr_data <= white & black & ascii;
				instr_wr <= '1';
				wait for CLK_PERIOD;
				instr_wr <= '0';
				wait until falling_edge(busy);

				-- get char for debug
				-- instr <= INSTR_GET_CHAR;
				-- instr_data <= white & black & ascii;
				-- instr_wr <= '1';
				-- wait for CLK_PERIOD;
				-- instr_wr <= '0';
				-- wait until falling_edge(busy);
				-- report "getChar: " & to_string(instr_result);

				-- increase ascii
				ascii := std_logic_vector(unsigned(ascii)+1);
				-- report "ascii: " & to_string(ascii);
				-- report "x: " & to_string(x);
				-- report "y: " & to_string(y);
			end loop;
		end loop;
		wait until vblank = '1';


		report "-----END-----";
		stop_clock <= true;
		wait;
	end process;



	generate_clk : process
	begin
		while not stop_clock loop
		clk <= '0', '1' after CLK_PERIOD / 2;
		wait for CLK_PERIOD;
	end loop;
	wait;
	end process;
end architecture;
