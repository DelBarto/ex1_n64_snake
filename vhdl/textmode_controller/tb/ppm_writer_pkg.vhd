library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package ppm_writer_pkg is

	component ppm_writer is
		generic (
			OUTPUT_DIRECTORY : string := "./"
		);
		port (
			nclk : in std_logic;
			grest : in std_logic;
			hd : in std_logic;
			vd : in std_logic;
			den : in std_logic;
			r : in std_logic_vector(7 downto 0);
			g : in std_logic_vector(7 downto 0);
			b : in std_logic_vector(7 downto 0)
		);
	end component;
end package;

