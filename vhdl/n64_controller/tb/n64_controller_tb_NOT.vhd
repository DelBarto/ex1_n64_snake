-- TASK 6: put your code here

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.n64_controller_pkg.all;

entity n64_controller_tb is
end entity;

architecture bench of n64_controller_tb is

	component n64_controller is
		generic (
		SYNC_STAGES : integer := 2;
		CLK_FREQ : integer := 50_000_000; --Hz
		REFRESH_TIMEOUT : integer := 400_000 --clock_cycles;
	);
	port (
		clk : in std_logic;
		res_n : in std_logic; 
		
		-- serial half-duplex data wire to the controller
		data : inout std_logic;
		
		-- button outputs
		button_state : out n64_buttons_t
	);
	end component;

	signal clk : std_logic;
	signal res_n : std_logic;
	signal data : std_logic;
	signal button_state : n64_buttons_t;

	CONSTANT CLK_PERIOD : time := 20 ns;
	signal stop_clock : boolean := false;
begin

	uut : n64_controller
		port map (
			clk	=> clk,
			res_n	=> res_n,
			button_state => button_state,
			data => data
		);


	stimulus : process
		variable seq : std_logic_vector(8 downto 0) := "000000000";
		CONSTANT start_seq : std_logic_vector(8 downto 0) := "0000000ZZ";
		variable btn_data : std_logic_vector(15 downto 0);
		constant matr : std_logic_vector(13 downto 0) := std_logic_vector(to_unsigned(11775806 mod 2 ** 14, 14));
		variable i : integer;
		variable x : std_logic_vector(7 downto 0) := std_logic_vector(to_unsigned(1, 8));
		variable y : std_logic_vector(7 downto 0) := std_logic_vector(to_signed(-1, 8));
		
	begin
		btn_data(15 downto 10) := matr(13 downto 8);
		btn_data(9 downto 0) := (others => '0');
		btn_data(7 downto 0) := matr(7 downto 0);
		res_n <= '0';
		report "btn_data: " & to_string(btn_data) & ", x: " & to_string(x) & ", y: " & to_string(y);
		wait for CLK_PERIOD;
		res_n <= '1';
		while not seq = start_seq loop 
			wait until falling_edge(data);
			wait for 1.5 us;
			if(data = 'Z') then
				seq := seq(7 downto 0) & data;
			else
				seq := seq(7 downto 0) & '0';
			end if;	
		end loop;
		report "starting sequence detected";
		data <= '0';
		wait for 3 us;
		for i in 15 downto 0 loop
			data <= '0';
			wait for 1 us;
			if(btn_data(i) = '1') then
				data <= '1';
			else
				wait for 2 us;
				data <= '1';
			end if;
			wait for 1 us;
		end loop;
		data <= '0';
		for i in 7 downto 0 loop
			wait for 1 us;
			if(x(i) = '1') then
				data <= '1';
			else
				wait for 2 us;
				data <= '1';
			end if;
			wait for 1 us;
		end loop;
		data <= '0';
		for i in 7 downto 0 loop
			wait for 1 us;
			if(y(i) = '1') then
				data <= '1';
			else
				wait for 2 us;
				data <= '1';
			end if;
			wait for 1 us;
		end loop;
		data <= 'Z';
		stop_clock <= true;
		wait;
	end process;

	generate_clk : process 
	begin
		while not stop_clock loop
			clk <= '0', '1' after CLK_PERIOD / 2;
			wait for CLK_PERIOD;
		end loop;
		wait;
	end process;
end architecture;
