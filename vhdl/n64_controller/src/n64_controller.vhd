
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.n64_controller_pkg.all;
use work.sync_pkg.all;

entity n64_controller is
	generic (
		SYNC_STAGES : integer ;
		-- CLK_FREQ : integer := 50_000_000;
		CLK_FREQ : integer; -- edit
		-- REFRESH_TIMEOUT : integer := 50_000_000/1_000*8
		REFRESH_TIMEOUT : integer -- edit
	);
	port (
		clk : in std_logic;
		res_n : in std_logic;

		data : inout std_logic;

		-- button outputs
		button_state : out n64_buttons_t
	);
end entity;

architecture rtl of n64_controller is

	-- https://groups.google.com/forum/#!msg/comp.lang.vhdl/eBZQXrw2Ngk/4H7oL8hdHMcJ
	function reverse_any_vector (a: in std_logic_vector)
		return std_logic_vector is
			variable result: std_logic_vector(a'RANGE);
			alias aa: std_logic_vector(a'REVERSE_RANGE) is a;
		begin
			for i in aa'RANGE loop
				result(i) := aa(i);
			end loop;
		return result;
	end; -- function reverse_any_vector


	type STATE_TYPE is (WAIT_TIMEOUT, POLL, SEND_LOW, SEND_HIGH, WAIT_SAMPLE, WAIT_FALLING_EDGE_DATA, SAMPLE_DATA);

	signal state : STATE_TYPE := WAIT_TIMEOUT;
	signal next_state : STATE_TYPE := WAIT_TIMEOUT;
	signal data_synced : std_logic;
	signal data_synced_old : std_logic;


	signal clk_cnt : integer := 0;
	signal clk_cnt_next : integer := 0;
	signal data_cnt : integer := 0;
	signal data_cnt_next : integer := 0;
	signal start_seq : std_logic_vector(8 downto 0) := "110000000";
	signal start_seq_next : std_logic_vector(8 downto 0) := "110000000";

	signal shiftreg_btns : std_logic_vector(31 downto 0)
					:= (others => '0');
	signal shiftreg_btns_next : std_logic_vector(31 downto 0)
					:= (others => '0');
	signal button_state_next : n64_buttons_t := N64_BUTTONS_RESET_VALUE;
begin


	sync_inst : sync
		generic map (
			SYNC_STAGES => SYNC_STAGES,
			RESET_VALUE => '0'
		)
		port map (
			clk      => clk,
			res_n    => res_n,
			data_in  => data,
			data_out => data_synced
		);


-- --------------------------------------------------------- --
--						NEXT STATE LOGIC					 --
-- --------------------------------------------------------- --
	next_state_logic : process(clk_cnt, state, start_seq, data_cnt,
								data_synced, data_synced_old)
		constant ONE_US : integer := CLK_FREQ/1_000_000;
		-- constant ONEANDAHALF_US : integer := CLK_FREQ/1_000_000 + CLK_FREQ/1_000_000/2;
	begin
		data_cnt_next <= data_cnt;
		clk_cnt_next <= clk_cnt;
		next_state <= state;
		start_seq_next <= start_seq;

		case state is
			when WAIT_TIMEOUT 			=>
				if (clk_cnt = REFRESH_TIMEOUT) then
					clk_cnt_next <= 0;
					data_cnt_next <= 0;
					next_state <= POLL;
				else
					clk_cnt_next <= clk_cnt + 1;
					next_state <= WAIT_TIMEOUT;
				end if;

			when POLL 					=>
				if (data_cnt < 9) then
					next_state <= SEND_LOW;
				else
					data_cnt_next <= 0;
					next_state <= WAIT_SAMPLE;
				end if;

			when SEND_LOW 				=>
				if (((start_seq(0) = '0') and (clk_cnt = 3*ONE_US)) or
					((start_seq(0) = '1') and (clk_cnt = ONE_US))) then
						clk_cnt_next <= 0;
						next_state <= SEND_HIGH;
				else
					clk_cnt_next <= clk_cnt + 1;
					next_state <= SEND_LOW;
				end if;

			when SEND_HIGH 				=>
				if (((start_seq(0) = '0') and (clk_cnt = ONE_US)) or
					((start_seq(0) = '1') and (clk_cnt = 3*ONE_US))) then
						start_seq_next <= start_seq(0) & start_seq(8 downto 1);
						clk_cnt_next <= 0;
						data_cnt_next <= data_cnt + 1;
						next_state <= POLL;
				else
						clk_cnt_next <= clk_cnt + 1;
						next_state <= SEND_HIGH;
				end if;

			when WAIT_SAMPLE 			=>
				-- if (clk_cnt = ONEANDAHALF_US) then
				if (clk_cnt = integer(1.5 * real(ONE_US))) then
					clk_cnt_next <= 0;
					next_state <= SAMPLE_DATA;
				else
					clk_cnt_next <= clk_cnt + 1;
					next_state <= WAIT_SAMPLE;
				end if;

			when WAIT_FALLING_EDGE_DATA =>
				if ((data_synced = '0') and (data_synced_old = '1')) then
					next_state <= WAIT_SAMPLE;
				end if;

			when SAMPLE_DATA 			=>
				if (data_cnt = 31) then
					next_state <= WAIT_TIMEOUT;
				else
					data_cnt_next <= data_cnt + 1;
					next_state <= WAIT_FALLING_EDGE_DATA;
				end if;
		end case;
	end process next_state_logic;


-- --------------------------------------------------------- --
--							SYNC PROCESS					 --
-- --------------------------------------------------------- --
	sync_proc : process(clk, res_n)
	begin
		if rising_edge(clk) then
			if (res_n = '0') then -- reset signals
				state <= WAIT_TIMEOUT;
				clk_cnt <= 0;
				data_cnt <= 0;
				start_seq <= "110000000";
				shiftreg_btns <= (others => '0');

			else
				state <= next_state;
				data_synced_old <= data_synced;
				clk_cnt <= clk_cnt_next;
				data_cnt <= data_cnt_next;
				start_seq <= start_seq_next;
				shiftreg_btns <= shiftreg_btns_next;
				button_state <= button_state_next;
			end if;
		end if;
	end process sync_proc;


-- --------------------------------------------------------- --
--							OUTPUT LOGIC					 --
-- --------------------------------------------------------- --
	output_logic : process(state, data_synced, shiftreg_btns)
	begin
		button_state_next <= button_state;
		shiftreg_btns_next <= shiftreg_btns;
		case state is
			when WAIT_TIMEOUT 			=>
				data <= 'Z';
				-- Write shiftreg_btns to the button_state
				button_state_next.btn_a       	<= shiftreg_btns(0);
				button_state_next.btn_b       	<= shiftreg_btns(1);
				button_state_next.btn_z       	<= shiftreg_btns(2);
				button_state_next.btn_start   	<= shiftreg_btns(3);
				button_state_next.btn_up      	<= shiftreg_btns(4);
				button_state_next.btn_down    	<= shiftreg_btns(5);
				button_state_next.btn_left    	<= shiftreg_btns(6);
				button_state_next.btn_right   	<= shiftreg_btns(7);
				-- unused shiftreg_btns(8)
				-- unused shiftreg_btns(9)
				button_state_next.btn_r       	<= shiftreg_btns(10);
				button_state_next.btn_l       	<= shiftreg_btns(11);
				button_state_next.btn_c_up    	<= shiftreg_btns(12);
				button_state_next.btn_c_down  	<= shiftreg_btns(13);
				button_state_next.btn_c_left  	<= shiftreg_btns(14);
				button_state_next.btn_c_right 	<= shiftreg_btns(15);
				button_state_next.as_x 			<= reverse_any_vector(shiftreg_btns(23 downto 16));
				button_state_next.as_y 			<= reverse_any_vector(shiftreg_btns(31 downto 24));

			when POLL 					=>
				data <= '0';

			when SEND_LOW 				=>
				data <= '0';

			when SEND_HIGH 				=>
				data <= 'Z';

			when WAIT_SAMPLE 			=>
				data <= 'Z';

			when WAIT_FALLING_EDGE_DATA =>
				data <= 'Z';

			when SAMPLE_DATA 			=>
				data <= 'Z';
				shiftreg_btns_next <= data_synced & shiftreg_btns(31 downto 1);
		end case;
	end process output_logic;


end architecture;
