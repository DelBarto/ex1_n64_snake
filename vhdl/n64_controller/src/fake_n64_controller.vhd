
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.n64_controller_pkg.all;
use work.sync_pkg.all;

entity fake_n64_controller is
	generic (
		SYNC_STAGES : integer := 2
	);
	port (
		clk : in std_logic;
		res_n : in std_logic; 
		
		-- use the board switches to emulate the snes buttons
		switches : std_logic_vector(13 downto 0);
		
		-- button outputs
		button_state : out n64_buttons_t
	);
end entity;

architecture rtl of fake_n64_controller is
	signal switches_sync : std_logic_vector(switches'range);
	signal clk_cnt_x : std_logic_vector(31 downto 0);
	signal clk_cnt_y : std_logic_vector(31 downto 0);
begin
	GENERATE_SYNCHRONIZERS : for i in switches'range  generate
		sync_inst : sync
		generic map (
			SYNC_STAGES => SYNC_STAGES,
			RESET_VALUE => '0'
		)
		port map (
			clk      => clk,
			res_n    => res_n,
			data_in  => switches(i),
			data_out => switches_sync(i)
		);
	end generate;
	
	button_state.btn_up      <= switches_sync(0);
	button_state.btn_down    <= switches_sync(1);
	button_state.btn_left    <= switches_sync(2);
	button_state.btn_right   <= switches_sync(3);
	button_state.btn_c_up    <= switches_sync(4);
	button_state.btn_c_down  <= switches_sync(5);
	button_state.btn_c_left  <= switches_sync(6);
	button_state.btn_c_right <= switches_sync(7);
	button_state.btn_a       <= switches_sync(8);
	button_state.btn_b       <= switches_sync(9);
	button_state.btn_start   <= switches_sync(10);
	button_state.btn_z       <= switches_sync(11);
	button_state.btn_r       <= switches_sync(12);
	button_state.btn_l       <= switches_sync(13);
	
	
	sync : process(clk, res_n)
	begin
		if (res_n = '0') then
			button_state.as_x <= (others=>'0');
			button_state.as_y <= (others=>'0');
			clk_cnt_x  <= (others=>'0');
			clk_cnt_y  <= (others=>'0');
		elsif (rising_edge(clk)) then
			if(unsigned(clk_cnt_x) = 12_500_00) then
				clk_cnt_x <= (others=>'0');
				button_state.as_x <= std_logic_vector(unsigned(button_state.as_x) + 1);
			else
				clk_cnt_x <= std_logic_vector(unsigned(clk_cnt_x) + 1);
			end if;
			
			if(unsigned(clk_cnt_y) = 11_500_00) then
				clk_cnt_y <= (others=>'0');
				button_state.as_y <= std_logic_vector(unsigned(button_state.as_y) - 1);
			else
				clk_cnt_y <= std_logic_vector(unsigned(clk_cnt_y) + 1);
			end if;
			
		end if;
	end process;
	
end architecture;


