library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package n64_controller_pkg is

	type n64_buttons_t is record
		btn_up     : std_logic;
		btn_down   : std_logic;
		btn_left   : std_logic;
		btn_right  : std_logic;
		btn_c_up   : std_logic;
		btn_c_down : std_logic;
		btn_c_left : std_logic;
		btn_c_right: std_logic;
		btn_start  : std_logic;
		btn_z      : std_logic;
		btn_l      : std_logic;
		btn_r      : std_logic;
		btn_a      : std_logic;
		btn_b      : std_logic;
		as_x       : std_logic_vector(7 downto 0);
		as_y       : std_logic_vector(7 downto 0);
	end record;

	constant N64_BUTTONS_RESET_VALUE : n64_buttons_t := (as_x => (others => '0'), as_y => (others => '0'), others=>'0');

	component n64_controller is
		generic (
			-- SYNC_STAGES : integer := 2;
			SYNC_STAGES : integer; --edit
			-- CLK_FREQ        : integer := 50000000;
			CLK_FREQ        : integer; -- edit
			REFRESH_TIMEOUT : integer
		);
		port(
			clk : in std_logic;
			data : inout std_logic;
			button_state : out n64_buttons_t;
			res_n : in std_logic
		);
	end component;

	component fake_n64_controller is
		generic (
			SYNC_STAGES : integer := 2
		);
		port (
			clk : in std_logic;
			res_n : in std_logic;
			switches : in std_logic_vector(13 downto 0);
			button_state : out n64_buttons_t
		);
	end component;


end package;
