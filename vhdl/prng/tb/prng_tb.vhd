-- TASK 5: put your code here
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
-- use ieee.numeric_bit_unsigned.all;


entity prng_tb is
end entity;



architecture bench of prng_tb is
	
	component prng is
		port (
			clk 		: in 	std_logic;
			res_n 		: in 	std_logic;
			load_seed 	: in	std_logic;
			seed   		: in	std_logic_vector(7 downto 0);
			en 			: in 	std_logic;
			prdata 		: out 	std_logic
		);
	end component;

	signal clk 			: std_logic;
	signal res_n 		: std_logic;
	signal load_seed 	: std_logic;
	signal seed   		: std_logic_vector(7 downto 0);
	signal en 			: std_logic;
	signal prdata 		: std_logic;
	
	constant CLK_PERIOD : time := 20 ns;
	signal stop_clock : boolean := false;
begin
	uut : prng
		port map (
			clk => clk,
			res_n => res_n,
			load_seed => load_seed,
			seed => seed,
			en => en,
			prdata => prdata 
		);

	stimulus : process
		VARIABLE pattern	: std_logic_vector(15 downto 0) := "0000000000000000";
		VARIABLE x_pat 		: std_logic_vector(15 downto 0) := STD_LOGIC_VECTOR(TO_UNSIGNED(16#AA80# + (1526618 mod 64), 16));
		VARIABLE counter 	: integer := 0;
	begin
	
		load_seed <= '0';
		en <= '1';
		
		WHILE NOT (pattern = x_pat) LOOP
			IF (en = '1') THEN     -- check if en is high
				pattern := pattern(14 downto 0) & prdata;  
				counter := counter + 1;
			END IF;
			wait until rising_edge(clk);
		END LOOP;
		
		report "required cycles: " & to_string(counter);
-- 		report "simulation Done";
		stop_clock <= true;
		wait;
	end process;


	generate_clk : process
	begin
		while not stop_clock loop
			clk <= '0', '1' after CLK_PERIOD / 2;
			wait for CLK_PERIOD;
		end loop;
		wait;
	end process;
end architecture;
